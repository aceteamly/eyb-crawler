﻿using EYBModel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    public class PdfBookDimensionsModel : DbBaseModel
    {
        public new int Id { get; set; }
        public int PdfBookRecipeId { get; set; }
        public int ContentType { get; set; }
        public float left { get; set; }
        public float top { get; set; }
        public float width { get; set; }
        public float height { get; set; }
        public string HTMLContent { get; set; }
        public int PageNumber { get; set; }
        public int PdfBookPageId { get; set; }
        public bool FetchData { get; set; }
        public bool IsFirstInMultipleContentType { get; set; }
        public int IdToNotDelete { get; set; } //Added by ND on dt. 08-12-2020
    }    

    public class PdfBookRecipe : DbBaseModel
    {
        public int PdfBookId { get; set; }
        public int PdfBookPageId { get; set; }
        public int RecipeNumber { get; set; }

        // Other Properties
        public string HTMLContent { get; set; }
        public int OrderNo { get; set; }
        public bool IndexerFlag { get; set; }
        public int PageNumber { get; set; }
        public string IndexerComment { get; set; }
        public int ContentType { get; set; }
    }

    public class IndexRecipeData
    {
        public int Id { get; set; }
        public bool IsNext { get; set; }
        public int PageNumber { get; set; }
        public bool IsJumpToPage { get; set; }
        public int OrderNo { get; set; }
        public bool IsContinueRecipe { get; set; }
        public int RecipeNumber { get; set; }
        public int BookPageNumber { get; set; }
        public bool IsJumpToBookPage { get; set; }

        public IndexRecipeData()
        {
            PageNumber = 1;
            IsJumpToPage = false;
            OrderNo = 0;
            IsContinueRecipe = false;
            RecipeNumber = 0;
            IsJumpToBookPage = false;
        }
    }

    public class NextRecipeAndVariant
    {
        public int NextVariantNo { get; set; }
        public int NextRecipeNumber { get; set; }
        public bool HasMultiple { get; set; }
        public bool HasVariant { get; set; }
        public int VariantBaseId { get; set; }
    }

    public class RecipeFilter
    {
        public int BookId { get; set; }
        public bool Refresh {get; set; }
    }

    public class ReorderRecipe
    {
        public int Id { get; set; }
        public int PageNumber { get; set; }
    }

    public class RecipeCommentModel
    {
        public int Id { get; set; }
        public string AdminComment { get; set; }
        public string IndexerComment { get; set; }
        public bool? IndexerFlag { get; set; }
        public int ModifiedBy { get; set; }
    }
}
