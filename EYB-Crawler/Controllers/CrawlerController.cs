﻿using EYB_Crawler.CrawlerConfigurations;
using EYBDal;
using EYBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngleSharp;
using System.Threading.Tasks;
using AngleSharp.Dom;
using Newtonsoft.Json.Linq;
using System.Web.Http.Results;
using System.Collections.Specialized;
using System.Configuration;

namespace EYB_Crawler.Controllers
{
    public class CrawlerController : ApiController
    {
        #region Automated Crawler Method
        [HttpGet]
        [Route("Crawler/CrawlSite")]
        public async Task<string> StartCrawlingAsync()
        {
            List<CrawlableSites> _crawlableSitesObj = new List<CrawlableSites>();
            _crawlableSitesObj = new CrawlerDal().Get(); // Gets list of websites that needs to be crawled

            var Config = AngleSharp.Configuration.Default.WithDefaultLoader();
            var Context = BrowsingContext.New(Config);
            string result = string.Empty;

            foreach (var value in _crawlableSitesObj)
            {
                
                if (value.BookId == BookKeys.Halfbakedharvest)
                {
                    result = await new Halfbakedharvest(Config, Context).ScrapSite(value, 0);
                }
                else if (value.BookId == BookKeys.Nigella)
                {
                    result = await new Nigella(Config, Context).ScrapSite(value, 0);
                }
                
                //else if (value.BookId == BookKeys.OhSheGlows)
                //{
                //    //result = await new OhSheGlows(Config, Context).ScrapSite(value, 0);
                //}
                //else if (value.BookId == BookKeys.JamieOliver)
                //{
                //    //result = await new JamieOliver(Config, Context).ScrapSite(value, 0);
                //}
                //if (value.BookId == BookKeys.Ottolenghi)
                //{
                //    //result = await new Ottolenghi(Config, Context).ScrapSite(value, 0);
                //}
            }
            return result;
        }
        #endregion

        #region Crawl by passing Link and respective ID
        [HttpGet]
        [Route("Crawler/CrawlSitebyId")]
        public async Task<string> CrawlSiteById(string url, int id)
        {
            var Config = AngleSharp.Configuration.Default.WithDefaultLoader();
            var Context = BrowsingContext.New(Config);
            CrawlableSites _crawlableSitesObj = new CrawlableSites();
            _crawlableSitesObj = new CrawlerDal().GetById(id); // Gets list of websites that needs to be crawled
            string result = string.Empty;

            if (id == BookKeys.Halfbakedharvest)
            {
                result = await new Halfbakedharvest(Config, Context).ScrapSite(_crawlableSitesObj, id, url);
                return Json(Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(result));
            }
            else if (id == BookKeys.Nigella)
            {
                result = await new Nigella(Config, Context).ScrapSite(_crawlableSitesObj, id, url);
                return Json(Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(result));
            }
            else
            {
                return "Please check provided URL and ID";
            }
            //else if (id == BookKeys.OhSheGlows)
            //{
            //    //result = await new OhSheGlows(Config, Context).ScrapSite(_crawlableSitesObj, id, url);
            //    return Json(Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(result));
            //}
            //else if (id == BookKeys.JamieOliver)
            //{
            //    //result = await new JamieOliver(Config, Context).ScrapSite(_crawlableSitesObj, id, url);
            //    return Json(Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(result));
            //}
            //if (id == BookKeys.Ottolenghi)
            //{
            //    //result = await new Ottolenghi(Config, Context).ScrapSite(_crawlableSitesObj, id, url);
            //    return Json(Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(result));
            //}
        }
        #endregion

        #region Scheduler to Sync Ingredients
        [HttpGet]
        [Route("Crawler/SyncIngredients")]
        public string SyncIngredients()
        {
            try
            {
                new CrawlerDal().SyncIngredients();
                new CrawlerDal().SyncIngredientVariations();
                return "Ingredients Synchronized Successfully";
            }
            catch (Exception ex)
            {
                new CrawlerDal().LogError(0, "Sync Scheduler", "", "", ex.ToString(), "Sync Scheduler");
                return "Error in synchronizing ingredients " + ex.ToString();
            }

        }
        #endregion

        #region ReTurm Angel Sharp Document
        public async System.Threading.Tasks.Task<IDocument> ReturnAngleSharpDocumentAsync(string url)
        {
            IDocument _iDocument;

            var Config = AngleSharp.Configuration.Default.WithDefaultLoader();
            var Context = BrowsingContext.New(Config);

            _iDocument = await Context.OpenAsync(url);

            return _iDocument;
        }
        #endregion
    }

    /// <summary>
    /// These Values should always be present in app settings
    /// </summary>
    public static class BookKeys
    {
        public static int Halfbakedharvest = int.Parse(ConfigurationManager.AppSettings["Halfbakedharvest"].ToString());
        public static int Nigella = int.Parse(ConfigurationManager.AppSettings["Nigella"].ToString());
    }
}