﻿using EYBCore;
using EYBModel;
using EYBModel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBDal
{
    public class RecipeCategoryDal
    {
        public int Update_Associate_Recipe_Categories(Associate_Recipe_CategoriesModel associate_Recipe_CategoriesModel)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("Id", associate_Recipe_CategoriesModel.Id);
            if (associate_Recipe_CategoriesModel.CourseIds.Count > 0)
                parameter.Add("CourseId", string.Join(",", associate_Recipe_CategoriesModel.CourseIds));
            if (associate_Recipe_CategoriesModel.EthnicityIds.Count > 0)
                parameter.Add("EthnicityId", string.Join(",", associate_Recipe_CategoriesModel.EthnicityIds));
            if (associate_Recipe_CategoriesModel.OccasionGroupsIds.Count > 0)
                parameter.Add("OccasionGroupsId", string.Join(",", associate_Recipe_CategoriesModel.OccasionGroupsIds));
            if (associate_Recipe_CategoriesModel.RecipeTypeIds.Count > 0)
                parameter.Add("RecipeTypeId", string.Join(",", associate_Recipe_CategoriesModel.RecipeTypeIds));
            if (associate_Recipe_CategoriesModel.SpecialDietIds.Count > 0)
                parameter.Add("SpecialDietId", string.Join(",", associate_Recipe_CategoriesModel.SpecialDietIds));
            if (associate_Recipe_CategoriesModel.AccompanimentIds.Count > 0)
                parameter.Add("AccompanimentId", string.Join(",", associate_Recipe_CategoriesModel.AccompanimentIds));
            parameter.Add("UserId", associate_Recipe_CategoriesModel.UserId);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<ReturnModel>("Update_Associate_Recipe_Categories", parameter);
                return result.ToList().FirstOrDefault().Id;
            }
        }

        public Associate_Recipe_CategoriesModel GetAllRecipeCategoryAndDetails(int recipeId,int pdfBookId)
        {
            Associate_Recipe_CategoriesModel obj = new Associate_Recipe_CategoriesModel();
            var parameter = new DbParameter().Parameters;
            parameter.Add("RecipeId", recipeId);
            parameter.Add("PdfBookId", pdfBookId);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSpMultiple("GetAllRecipeCategoryAndDetails", parameter);
                obj = result.Read<Associate_Recipe_CategoriesModel>().FirstOrDefault();
                if (obj == null)
                    obj = new Associate_Recipe_CategoriesModel();
                if (!string.IsNullOrEmpty(obj.CourseId))
                    obj.CourseIds = obj.CourseId.Split(',').Select(x => int.Parse(x)).ToList();
                if (!string.IsNullOrEmpty(obj.EthnicityId))
                    obj.EthnicityIds = obj.EthnicityId.Split(',').Select(x => int.Parse(x)).ToList();
                if (!string.IsNullOrEmpty(obj.OccasionGroupsId))
                    obj.OccasionGroupsIds = obj.OccasionGroupsId.Split(',').Select(x => int.Parse(x)).ToList();
                if (!string.IsNullOrEmpty(obj.RecipeTypeId))
                    obj.RecipeTypeIds = obj.RecipeTypeId.Split(',').Select(x => int.Parse(x)).ToList();
                if (!string.IsNullOrEmpty(obj.SpecialDietId))
                    obj.SpecialDietIds = obj.SpecialDietId.Split(',').Select(x => int.Parse(x)).ToList();
                if (!string.IsNullOrEmpty(obj.AccompanimentId))
                    obj.AccompanimentIds = obj.AccompanimentId.Split(',').Select(x => int.Parse(x)).ToList();
                obj.Course = result.Read<Select2Model>().ToList();
                obj.Ethnicity = result.Read<Select2Model>().ToList();
                obj.OccasionGroups = result.Read<Select2Model>().ToList();
                obj.RecipeType = result.Read<Select2Model>().ToList();
                obj.SpecialDiet = result.Read<Select2Model>().ToList();
                //if (!isVarientRecipe)
                //{
                    obj.Accompaniment = result.Read<Select2Model>().ToList();
                //}
                return obj;
            }
        }

        //Added By ABP on 05-10-2020
        //Gets All Book Categories 
        public Book_CategoriesModel GetAllBookCategory(int pdfBookId)
        {
            Book_CategoriesModel obj = new Book_CategoriesModel();
            var parameter = new DbParameter().Parameters;
            parameter.Add("PdfBookId", pdfBookId);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSpMultiple("GetAllBookCategoryAndDetails", parameter);
                obj = result.Read<Book_CategoriesModel>().FirstOrDefault();
                if (obj == null)
                    obj = new Book_CategoriesModel();
                if (!string.IsNullOrEmpty(obj.CourseId))
                    obj.CourseIds = obj.CourseId.Split(',').Select(x => int.Parse(x)).ToList();
                if (!string.IsNullOrEmpty(obj.EthnicityId))
                    obj.EthnicityIds = obj.EthnicityId.Split(',').Select(x => int.Parse(x)).ToList();
                if (!string.IsNullOrEmpty(obj.OccasionGroupsId))
                    obj.OccasionGroupsIds = obj.OccasionGroupsId.Split(',').Select(x => int.Parse(x)).ToList();
                if (!string.IsNullOrEmpty(obj.RecipeTypeId))
                    obj.RecipeTypeIds = obj.RecipeTypeId.Split(',').Select(x => int.Parse(x)).ToList();
                if (!string.IsNullOrEmpty(obj.SpecialDietId))
                    obj.SpecialDietIds = obj.SpecialDietId.Split(',').Select(x => int.Parse(x)).ToList();

                obj.Course = result.Read<Select2Model>().ToList();
                obj.Ethnicity = result.Read<Select2Model>().ToList();
                obj.OccasionGroups = result.Read<Select2Model>().ToList();
                obj.RecipeType = result.Read<Select2Model>().ToList();
                obj.SpecialDiet = result.Read<Select2Model>().ToList();

                return obj;
            }
        }

    }
}
