﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    public class PdfBookRecipeModel
    {
        public int id { get; set; }
        public List<int> RecipeTypeIds { get; set; }
        public List<int> EthnicityIds { get; set; } 
        public List<int> CourseIds { get; set; }
        public List<int> OccasionGroupsIds { get; set; }
        public List<int> SpecialDietIds { get; set; }

        public string CourseId { get; set; }
        public string EthnicityId { get; set; }
        public string OccasionGroupsId { get; set; }
        public string RecipeTypeId { get; set; }
        public string SpecialDietId { get; set; }

        public PdfBookRecipeModel()
        {
            CourseIds = new List<int>();
            EthnicityIds = new List<int>();
            OccasionGroupsIds = new List<int>();
            RecipeTypeIds = new List<int>();
            SpecialDietIds = new List<int>();
        }

    }
}
