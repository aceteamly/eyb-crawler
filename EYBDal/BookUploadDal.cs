﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EYBCore;
using EYBModel;
using EYBModel.Helper;

namespace EYBDal
{
    public class BookUploadDal
    {
        #region PDFBook
        public int InsertPdfBook(EYBModel.PdfBookModel pdfBook)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("ContainerName", pdfBook.ContainerName);
            parameter.Add("PdfName", pdfBook.PdfName);
            parameter.Add("ProcessStatus", pdfBook.ProcessStatus);
            parameter.Add("CreatedBy", pdfBook.CreatedBy);
            parameter.Add("IndexingStatus", pdfBook.IndexingStatus);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<ReturnModel>("sp_Insert_PdfBook", parameter);
                return result.First().Id;
            }
        }

        public List<EYBModel.PdfBookModel> GetPdf()
        {

            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<EYBModel.PdfBookModel>("sp_GetPdfForProcessing");
                return result.ToList();
            }
        }

        public PdfBookModel PdfBook_Select(int id)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("Id", id);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<EYBModel.PdfBookModel>("sp_PdfBook_Select", parameter);
                return result.ToList().FirstOrDefault();
            }
        }

        public void DataTransfterUpdateUnit()
        {
            var parameter = new DbParameter().Parameters;
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<int>("DataTransfterUpdateUnit", parameter);
            }
        }

        public List<EYBModel.BookListModel> GetBookList(BookSearchModel model)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("PageNumber", model.PageNumber);
            parameter.Add("PageSize", model.PageSize);
            parameter.Add("BookName", model.BookName);
            parameter.Add("ISBN", model.ISBN);
            parameter.Add("AuthorName", model.AuthorName);
            parameter.Add("IsPending", model.IsPending);
            parameter.Add("IsCompleted", model.IsCompete);
            parameter.Add("ProcessingStatus", model.ProcessingStatus);
            parameter.Add("OrderBy", model.OrderBy);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<EYBModel.BookListModel>("Sp_GetBookList", parameter);
                return result.ToList();
            }
        }

        public List<PdfBookModel> GetAllBook_ForTransfer()
        {
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<EYBModel.PdfBookModel>("GetAllBook_ForTransfer");
                return result.ToList();
            }
        }


        public ReturnModel DataTransfer(int id)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("BookId", id);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<ReturnModel>("DataTransfer", parameter);
                return result.FirstOrDefault();
            }
        }

        public ReturnModel CheckPDFBookLocked(int id)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("Id", id);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<ReturnModel>("CheckPDFBookLocked", parameter);
                return result.FirstOrDefault();
            }
        }
        public int CheckAllIngredientApproved(int id)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("Id", id);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<ReturnModel>("CheckAllIngredientApproved", parameter);
                return result.FirstOrDefault().Id;
            }
        }

        #endregion

        #region PDFUpdateStatus

        public int PDFUpdateStatus(EYBModel.PdfBookModel pdfBook)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("Id", pdfBook.Id);
            parameter.Add("ThumbnailName", pdfBook.ThumbnailName);
            parameter.Add("ProcessStatus", pdfBook.ProcessStatus);

            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<ReturnModel>("Sp_PdfUpdateStatus", parameter);
                return result.First().Id;
            }
        }

        public int CheckBookIndexed(int id)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("Id", id);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<ReturnModel>("CheckBookIndexed", parameter);
                return result.First().Id;
            }
        }
        public ReturnModel CheckPDFBookStatusLocked(int id)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("Id", id);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<ReturnModel>("CheckPDFBookStatusLocked", parameter);
                return result.FirstOrDefault();
            }
        }
        #endregion

        #region Metadata
        public int UpdateMetadata(EYBModel.PdfBookMetaData model)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("Id", model.MetaDataId);
            parameter.Add("BookName", model.BookName);
            parameter.Add("ISBN", model.ISBN);
            parameter.Add("AuthorName", model.AuthorName);
            parameter.Add("ModifieddBy", model.ModifieddBy);
            parameter.Add("MasterBookId", model.MasterBookId);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<ReturnModel>("Sp_UpdateMetadata", parameter);
                return result.First().Id;
            }
        }
        public dynamic SearchMetaDataByISBN(string ISBN)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("ISBN", ISBN);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<PdfBookMetaData>("sp_Search_Book_By_ISBN", parameter);
                return result.FirstOrDefault();
            }
        }
        public dynamic GetMetaDataById(int Id)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("Id", Id);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<PdfBookMetaData>("sp_Get_MetaData_By_Id", parameter);
                return result.FirstOrDefault();
            }
        }
        #endregion

        #region Insert PdfBookHtml Data

        public void InsertPdfBookHtml(EYBModel.BookPdfHtmlModel Bookexclude)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("PdfBookId", Bookexclude.PdfBookId);
            parameter.Add("PageNames", Bookexclude.PageName);
            parameter.Add("CreatedBy", Bookexclude.CreatedBy);

            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<ReturnModel>("sp_Insert_PdfBookPages", parameter);
            }
        }

        #endregion

        #region Get PdfBookHtml List

        public List<EYBModel.BookPdfHtmlModel> GetBookHtmlList(int PdfBookId)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("PdfBookId", PdfBookId);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<EYBModel.BookPdfHtmlModel>("sp_GetPdfHtmlList", parameter);
                return result.ToList();
            }
        }

        #endregion

        #region Update Exculde Status

        public int UpdatePdfHtmlExculde(int id, bool isExculde, int pdfBookId, int modifiedBy)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("Id", id);
            parameter.Add("IsExculde", isExculde);
            parameter.Add("PdfBookId", pdfBookId);
            parameter.Add("ModifieddBy", modifiedBy);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<BookPdfHtmlModel>("Sp_UpdatePdfBookPages", parameter);
                return result.First().ExcludeCount;
            }
        }

        public int DeleteRecipeAndExcluePage(int id, int pdfBookId, int modifiedBy)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("Id", id);
            parameter.Add("PdfBookId", pdfBookId);
            parameter.Add("ModifiedBy", modifiedBy);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<BookPdfHtmlModel>("DeleteRecipeAndExcluePage", parameter);
                return result.First().ExcludeCount;
            }
        }
        #endregion

        #region Get Non ExculdePage List

        public List<EYBModel.BookPdfHtmlModel> GetNonExculdePageList(int PdfBookId)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("PdfBookId", PdfBookId);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<EYBModel.BookPdfHtmlModel>("sp_GetNonExculdePageList", parameter);
                return result.ToList();
            }
        }

        #endregion


    }
}
