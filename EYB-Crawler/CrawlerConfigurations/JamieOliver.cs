﻿using AngleSharp;
using AngleSharp.Dom;
using EYB_Crawler.HelperMethods;
using EYBDal;
using EYBModel;
using EYBWeb.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;



namespace EYB_Crawler.CrawlerConfigurations
{
    public class JamieOliver
    {
        public readonly IConfiguration _config;
        public readonly IBrowsingContext _context;

        public JamieOliver(IConfiguration config, IBrowsingContext context)
        {
            _config = config;
            _context = context;
        }

        public async Task<string> ScrapSite(CrawlableSites _obj, int ID, string url = null)
        {
            IDocument mainDocument;
            string lastCrawledPageUrl = string.Empty;
            string RecipeLink = string.Empty;
            string crawlStatus = string.Empty;

            // Before launch. We will pass each page url to get the recipes from the page.
            // then index page will be used to get the latest recipe.

            try
            {
                if (!string.IsNullOrEmpty(url) && !string.IsNullOrWhiteSpace(url) && ID != 0)
                {
                    mainDocument = await ReturnAngleSharpDocumentAsync(url);
                    lastCrawledPageUrl = _obj.CrawlerUrl;
                    RecipeLink = url;
                    bool status = new CrawlerDal().IfUrlAlreadyCrawled(RecipeLink, ID);
                    if (!status)
                    {
                        crawlStatus = await CrawlUrlAsync(RecipeLink, _obj, ID);
                    }
                    else
                    {
                        crawlStatus = "This url has already been crawled";
                    }
                }

                else
                {
                    mainDocument = await ReturnAngleSharpDocumentAsync(_obj.CrawlerUrl);
                    string LastCrawledRecipeName = string.Empty;
                    int TotalPageRecipes = 0;
                    var mainSiteMapDiv = mainDocument.QuerySelector(".html-sitemap");
                    var siteMapRecipeLink = mainSiteMapDiv.QuerySelectorAll("a").Where(s => s.Attributes["href"].Value == "/recipes/").FirstOrDefault();
                    var recipeLinks = siteMapRecipeLink.NextElementSibling.Children.ToList();
                    lastCrawledPageUrl = _obj.CrawlerUrl;
                    bool status = false;
                    foreach (var link in recipeLinks)
                    {
                        try
                        {
                            RecipeLink = link.QuerySelector("a").Attributes["href"].Value;
                            status = new CrawlerDal().IfUrlAlreadyCrawled(RecipeLink,_obj.BookId);
                           if (!status)
                            {
                                LastCrawledRecipeName = RecipeLink;
                                await CrawlUrlAsync(RecipeLink, _obj, 0);
                                TotalPageRecipes++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        catch (Exception ex)
                        {
                            new CrawlerDal().LogError(_obj.Id, _obj.WebsiteName, "", RecipeLink, ex.ToString(),"Jamie Oliver Scrap Data");
                            new CrawlerDal().InsertCrawlerRunStatus(_obj.Id, false, "Jamie Oliver Scrap Data");                            
                            crawlStatus = "Error in crawling. Please check url povided or check error logs";
                            //return crawlStatus;
                        }
                    }

                    if (!status)
                    {
                        new CrawlerDal().UpdateCrawlableSiteConfiguration(LastCrawledRecipeName, Convert.ToString(TotalPageRecipes.ToString()), lastCrawledPageUrl, _obj.BookId);
                    }

                    new CrawlerDal().InsertCrawlerRunStatus(_obj.Id, true, "Jamie Oliver Scrap Data");
                    crawlStatus = "Your url has been crawled successfully. Please check main site to see changes";
                    return crawlStatus;
                }
                
            }
            catch(Exception ex)
            {
                new CrawlerDal().LogError(_obj.Id, _obj.WebsiteName, "", RecipeLink, ex.ToString(), "Jamie Oliver Scrap Data");
                crawlStatus = "Error in crawling. Please check url povided or check error logs";
            }

            return crawlStatus;

        }

        #region method returning AngleSharp Document
        public async Task<IDocument> ReturnAngleSharpDocumentAsync(string url)
        {
            IDocument _iDocument;

            _iDocument = await _context.OpenAsync(url);

            return _iDocument;
        }
        #endregion

        #region main method for crawling

        public async Task<string> CrawlUrlAsync(string RecipeLink, CrawlableSites _obj, int Id)
        {
            string crawlerStatus = string.Empty;
            bool isCrawlSuccess = false;

            if (!string.IsNullOrEmpty(RecipeLink) && !string.IsNullOrWhiteSpace(RecipeLink))
            {
                string recipeTitle = string.Empty;
                string LastCrawledRecipeName = string.Empty;
                try
                {
                    #region Variable Declarations
                    recipeTitle = string.Empty;
                    string servingSize = string.Empty;
                    string photoUrl = string.Empty;
                    string recipeVideoUrl = string.Empty;
                    List<string> ingredients = new List<string>();
                    List<string> detectedIngredients = new List<string>();
                    List<string> unDetectedIngredients = new List<string>();
                    var recipeIngredientsData = new DataTable();
                    string undetectedIngredients = string.Empty;
                    #endregion                    

                    var RecipeDocument = await _context.OpenAsync(RecipeLink);

                    #region Title Identifier

                    string scriptDataJson = RecipeDocument.Scripts.Where(s => s.Type == "application/ld+json").FirstOrDefault().InnerHtml;
                    if(JToken.Parse(scriptDataJson).SelectToken("video") != null)
                    {
                        recipeVideoUrl = JToken.Parse(scriptDataJson).SelectToken("video").SelectToken("contentUrl").ToString();
                    }                   

                    if (RecipeDocument.QuerySelector(_obj.TitleIdentifier) != null)
                    {
                        recipeTitle = RecipeDocument.QuerySelector(_obj.TitleIdentifier).TextContent;
                        recipeTitle = StringManipulation.RemoveSpaces(recipeTitle);
                        recipeTitle = StringManipulation.StripHTML(recipeTitle);
                        recipeTitle = StringManipulation.TitleCase(recipeTitle);
                        LastCrawledRecipeName = recipeTitle;
                    }
                    else
                    {
                        recipeTitle = null;
                    }
                    #endregion

                    #region Serving Identifier
                    if (RecipeDocument.QuerySelector(_obj.ServingSizeIdentifier) != null)
                    {
                        servingSize = RecipeDocument.QuerySelector(_obj.ServingSizeIdentifier).NextSibling.TextContent;
                        servingSize = StringManipulation.RemoveSpaces(servingSize);
                        servingSize = StringManipulation.StripHTML(servingSize);
                    }
                    else
                    {
                        servingSize = null;
                    }
                    #endregion

                    #region Image Identifier
                    if (RecipeDocument.QuerySelector(_obj.PhotosIdentifier) != null)
                    {
                        if (RecipeDocument.QuerySelector(_obj.PhotosIdentifier).HasAttribute("src"))
                        {
                            string urlToCheck = string.Empty;
                            urlToCheck = RecipeDocument.QuerySelector(_obj.PhotosIdentifier).Attributes["src"].Value;
                            if (UriMethods.CheckUrlFormat(urlToCheck))
                            {
                                photoUrl = RecipeDocument.QuerySelector(_obj.PhotosIdentifier).Attributes["src"].Value;
                            }
                            else
                            {
                                photoUrl = null;
                            }

                        }
                        else
                        {
                            photoUrl = null;
                        }
                    }
                    else
                    {
                        photoUrl = null;
                    }
                    #endregion

                    //int recipeId = new CrawlerDal().CreateRecipe(recipeTitle, RecipeLink, servingSize, photoUrl, _obj.BookId, recipeVideoUrl,null);
                    int recipeId = 0;
                    if (!string.IsNullOrEmpty(recipeTitle))
                    {
                        recipeId = new CrawlerDal().CreateRecipe(recipeTitle, RecipeLink, servingSize, photoUrl, _obj.BookId, recipeVideoUrl, null,null);
                    }

                    if (recipeId != 0)
                    {
                        #region Ingredient Identifier
                        if (RecipeDocument.QuerySelector(_obj.IngredientIdentifier) != null)
                        {
                            var list = RecipeDocument.QuerySelectorAll(_obj.IngredientIdentifier);
                            foreach (var value1 in list)
                            {
                                string ingredienttext = string.Empty;
                                ingredienttext = StringManipulation.RemoveSpaces(value1.TextContent);
                                ingredienttext = StringManipulation.StripHTML(ingredienttext);
                                ingredients.Add(ingredienttext);

                            }

                            ingredients = ingredients.Distinct().ToList();
                            //ingredients = StringManipulation.RemoveExcludedIngredients(ingredients);

                            List<IngredientModel> ingredientslist = new Ingredient().GetIngredientsKeyword(ingredients);
                            var rowGroupedIngredients = ingredientslist.GroupBy(r => r.RowId);

                            recipeIngredientsData.Columns.Add("DisplayQuantity");
                            recipeIngredientsData.Columns.Add("Quantity");
                            recipeIngredientsData.Columns.Add("UnitId");
                            recipeIngredientsData.Columns.Add("IngredientId");
                            recipeIngredientsData.Columns.Add("ReferenceId");
                            recipeIngredientsData.Columns.Add("SortOrder");
                            recipeIngredientsData.Columns.Add("RecipeId");

                            int i = 0;

                            foreach (var data in rowGroupedIngredients)
                            {
                                int ingredientId = 0;
                                string quantity = string.Empty;
                                string displayQuantity = string.Empty;
                                int? referenceId = null;
                                int? unitId = null;
                                List<int> _detectedId = new List<int>();

                                foreach (var subData in data)
                                {
                                    if (subData.KeywordId != 0)
                                    {
                                        _detectedId.Add(subData.KeywordId);
                                        //ingredientId = subData.KeywordId;
                                    }
                                    if (subData.Id != 0 && subData.Id != -1)
                                    {
                                        unitId = subData.Id;
                                    }
                                    if (!string.IsNullOrEmpty(subData.Quantity) && !string.IsNullOrWhiteSpace(subData.Quantity))
                                    {
                                        displayQuantity = subData.Quantity;
                                        quantity = subData.Quantity;
                                    }

                                    if (subData.ReferenceId != 0)
                                    {
                                        referenceId = subData.ReferenceId;
                                    }

                                    if (!string.IsNullOrEmpty(subData.Content) && !string.IsNullOrWhiteSpace(subData.Content))
                                    {
                                        detectedIngredients.Add(subData.Content);
                                    }
                                }

                                ingredientId = _detectedId.FirstOrDefault();
                                

                                if (!recipeIngredientsData.AsEnumerable().Any(row => ingredientId.ToString() == row.Field<string>("IngredientId")) && ingredientId != 0)
                                {
                                    recipeIngredientsData.Rows.Add(displayQuantity, quantity, unitId, ingredientId, referenceId, i, recipeId);
                                    i = i + 9;
                                }

                            }
                            #region detect undetected ingredients
                            foreach (var ingredient in detectedIngredients)
                            {
                                string result = ingredients.FirstOrDefault(s => s.ToLower().Contains(ingredient.ToLower()));
                                if (!string.IsNullOrEmpty(result) && !string.IsNullOrWhiteSpace(result))
                                {
                                    ingredients.Remove(result);
                                }
                            }

                            foreach (var undetectedIngredient in ingredients)
                            {
                                undetectedIngredients += undetectedIngredient + "; ";
                            }
                            #endregion
                            if (recipeIngredientsData != null)
                            {
                                if (recipeIngredientsData.Rows.Count > 0)
                                {
                                    int success = new CrawlerDal().InsertIngredients(recipeIngredientsData, recipeId);
                                }
                            }
                            if (!string.IsNullOrWhiteSpace(undetectedIngredients) && !string.IsNullOrEmpty(undetectedIngredients))
                            {
                                new CrawlerDal().UpdateudetectedINgredients(undetectedIngredients, recipeId);
                            }
                        }
                        #endregion

                        isCrawlSuccess = true;
                    }
                }
                catch (Exception ex)
                {
                    new CrawlerDal().LogError(_obj.Id, _obj.WebsiteName, recipeTitle, RecipeLink, ex.ToString(), "Jamie Oliver Scrap Data");
                    crawlerStatus = "Error in crawling. Please check url povided or check error logs";
                    isCrawlSuccess = false;
                    //return "Error in crawling. Please check url povided or check error logs";
                }

                if (Id != 0 && isCrawlSuccess)
                {
                    new CrawlerDal().UpdateCrawlableSiteConfiguration(LastCrawledRecipeName,"1", RecipeLink, _obj.BookId);
                    new CrawlerDal().InsertCrawlerRunStatus(_obj.Id, true, "Jamie Oliver Scrap Data");
                    crawlerStatus = "Your url has been crawled successfully. Please check main site to see changes";
                    
                }
            }
            else
            {
                if (Id != 0)
                {
                    new CrawlerDal().InsertCrawlerRunStatus(_obj.Id, false, "Jamie Oliver Scrap Data");
                    Log.RunInfo("Jamie Oliver did not ran successfully");
                    crawlerStatus = "Error in crawling. Please check url povided or check error logs";
                }
            }

            return crawlerStatus;
        }

        #endregion


    }
}