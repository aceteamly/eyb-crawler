﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    public class Select2Model
    {
        // Do not capital first charcter
        public int id { get; set; }
        public string text { get; set; }
    }
}
