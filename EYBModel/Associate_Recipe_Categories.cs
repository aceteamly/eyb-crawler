﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    public class Associate_Recipe_CategoriesModel
    {
        public int Id { get; set; }
        public List<int> CourseIds { get; set; }
        public List<int> EthnicityIds { get; set; }
        public List<int> OccasionGroupsIds { get; set; }
        public List<int> RecipeTypeIds { get; set; }
        public List<int> SpecialDietIds { get; set; }
        public List<int> AccompanimentIds { get; set; }
        public string CourseId { get; set; }
        public string EthnicityId { get; set; }
        public string OccasionGroupsId { get; set; }
        public string RecipeTypeId { get; set; }
        public string SpecialDietId { get; set; }
        public string AccompanimentId { get; set; }
        public int UserId { get; set; }

        public List<Select2Model> Course { get; set; }
        public List<Select2Model> Ethnicity { get; set; }
        public List<Select2Model> OccasionGroups { get; set; }
        public List<Select2Model> RecipeType { get; set; }
        public List<Select2Model> SpecialDiet { get; set; }
        public List<Select2Model> Accompaniment { get; set; }

        public Associate_Recipe_CategoriesModel()
        {
            CourseIds = new List<int>();
            EthnicityIds = new List<int>();
            OccasionGroupsIds = new List<int>();
            RecipeTypeIds = new List<int>();
            SpecialDietIds = new List<int>();
            AccompanimentIds = new List<int>();
        }

    }
}
