﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    public class UpdateIngredientsViewModel
    {
        public int ID { get; set; }
        public string OnlineUrl { get; set; }
    }

    public class ExistingIngredientModel
    {
        public string Title { get; set; }
    }
}
