﻿using EYBModel.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace EYBModel
{
    public class LoginModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = AppString_Validation.Req_UserName)]
        public string UserName { get; set; }

        [Required(ErrorMessage = AppString_Validation.Req_Password)]
        public string Password { get; set; }
    }
}