﻿using EYB_Crawler.HelperMethods;
using EYBDal;
using EYBModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace EYBWeb.Helpers
{

    public class Ingredient
    {
        readonly string HTMLName = string.Empty;
        readonly string strCurrentFolderName = string.Empty;
        static readonly string specialChar = @"([<>?*\]),@%é&-_+=!⁄|";
        static readonly string regexFraction = @"\d+(\.\d+)?/\d+(\.\d+)?"; // Added by ABP on 20-10-2020 regarding https://trello.com/c/O2SYZTUq/17-fractions-not-importing-correctly
        string rxCommon = @"(([0-9])+(##key##))|" +
             "(([0-9.]+ (to|or)* [0-9.]+[ ])+(##key##)+(?![a-zA-Z]))|" +
             "(([0-9-. ]+( to | or |\\/|to)*|[^" + specialChar + "[a-z A-Z 0-9])+(##key##))+(?![a-zA-Z])|" +
             "(([0-9.]|[^" + specialChar + "[a-z A-Z 0-9])+[ ]+(##key##)+(?![a-zA-Z]))";
        static string rxPageIndex = @"(\b(##key##)\b)+[ ]?[0-9]+";
        List<SpecialChar> specialChars = new SpecialChar().Intialize();

        public List<IngredientModel> GetIngredientsKeyword(List<string> ingredients)
        {
            #region Search Ingredient Keywords
            List<IngredientModel> ingredientModels = new List<IngredientModel>();
            if (ingredients != null && ingredients.Count > 0)
            {
                DataTable dtIngredient = PrepareParam(ingredients);
                if (dtIngredient?.Rows.Count > 0)
                {
                    ingredientModels = GetIngredientUnit(dtIngredient);
                }
            }
            #endregion Search Ingredient Keywords

            return ingredientModels;
        }

        private DataTable PrepareParam(List<string> arrIngredient)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RowID", typeof(int));
            dt.Columns.Add("IngredientText", typeof(string));

            if (arrIngredient?.Count > 0)
            {
                for (int i = 0; i < arrIngredient.Count; i++)
                {
                    string ingredientText = Convert.ToString(arrIngredient[i].Replace("⁄", "/").Replace("\t", "").Replace("—", "").Replace("\a ", "").Replace("\a", "").Replace(" to ", "-").Replace(" or ", "-").Replace("• ", ""));
                    if (!string.IsNullOrEmpty(ingredientText) && !string.IsNullOrWhiteSpace(ingredientText))
                    {
                        DataRow dr = dt.NewRow();
                        dr["RowID"] = i + 1;
                        dr["IngredientText"] = ingredientText;
                        dt.Rows.Add(dr);
                    }
                }
            }

            return dt;
        }

        private List<IngredientModel> GetIngredientUnit(DataTable dtIngredient)
        {
            List<IngredientModel> unitModels = new List<IngredientModel>();
            List<KeywordModel> ingredientKeywords = new IndexDal().GetIngredientsKeyword(dtIngredient);
            ingredientKeywords = StringManipulation.RemoveExcludedIngredients(ingredientKeywords);
            List<UnitModel> lstIngredientKey = new UnitDal().GetUnits(true, true);
            UnitModel pcsUnit = lstIngredientKey.FirstOrDefault(t => t.Id == -1);

            for (int i = 0; i < dtIngredient.Rows.Count; i++)
            {
                string strIngredientText = Convert.ToString(dtIngredient.Rows[i]["IngredientText"]);
                foreach (var item in lstIngredientKey)
                {
                    bool pageNumber = false;
                    if (!string.IsNullOrEmpty(strIngredientText) && !string.IsNullOrWhiteSpace(strIngredientText))
                    {
                        string strRegex = rxCommon.Replace("##key##", item.Name);
                        if (item.Name.ToLower().Equals("page") || item.Name.ToLower().Equals("pages"))
                        {
                            strRegex = rxPageIndex.Replace("##key##", item.Name);
                            pageNumber = true;
                        }
                        MatchCollection regexList = Regex.Matches(strIngredientText, strRegex, RegexOptions.IgnoreCase);
                        foreach (Match regex in regexList)
                        {
                            string strMatchIngredients = Convert.ToString(regex);
                            if (!string.IsNullOrEmpty(strMatchIngredients) && !string.IsNullOrWhiteSpace(strMatchIngredients))
                            {
                                string Quantity = ParseQuantity(!string.IsNullOrEmpty(item.Name) ? strMatchIngredients.ToLower().Replace(item.Name, "").Trim() : string.Empty);
                                //Change Start
                                //Added by ABP on 20-10-2020 regarding https://trello.com/c/O2SYZTUq/17-fractions-not-importing-correctly
                                if (!string.IsNullOrEmpty(Quantity) && !string.IsNullOrWhiteSpace(Quantity) )
                                {
                                    Match regFractions = Regex.Match(Quantity, regexFraction, RegexOptions.IgnoreCase);
                                    string matchFraction = Convert.ToString(regFractions);
                                    if (!string.IsNullOrEmpty(matchFraction))
                                    {
                                        long nResult, dResult;
                                        string[] fractions = Quantity.Split('/');
                                        if (long.TryParse(fractions[0], out nResult) && long.TryParse(fractions[1], out dResult))
                                        {
                                            if (nResult > dResult)
                                            {
                                                string firstdigit = nResult.ToString().Substring(0, 1);
                                                Quantity = firstdigit + " " + nResult.ToString().Substring(1, 1) + '/' + dResult;

                                                decimal val = FractionToDouble(Quantity);
                                                Quantity = val.ToString();
                                            }
                                        }

                                    }
                                }
                                //Change End
                                if (!string.IsNullOrEmpty(Quantity) && !string.IsNullOrWhiteSpace(Quantity))
                                {
                                    int unitId = item.Id;
                                    string unitName = item.Name;
                                    if (item.BaseId != 0)
                                    {
                                        unitId = item.BaseId;
                                        unitName = lstIngredientKey.FirstOrDefault(x => x.Id == unitId).Name;
                                    }

                                    IngredientModel unitModel = new IngredientModel
                                    {
                                        Id = unitId,
                                        Name = unitName,
                                        RowId = i,
                                        Quantity = Quantity,
                                        IsPageNumber = pageNumber,
                                        Index = regex.Index// Get text index for ordering

                                    };
                                    unitModels.Add(unitModel);
                                }
                            }
                        }
                    }
                }
                if (ingredientKeywords != null)
                {
                    List<KeywordModel> keywords = ingredientKeywords.Where(x => x.LineID == i + 1).ToList();

                    //If UOM does not found for the particular statement.
                    if (!unitModels.Any(t => t.RowId == i))
                    {
                        foreach (var keyword in keywords)
                        {
                            string strRegex = rxCommon.Replace("##key##", keyword.Keyword);
                            int referenceid = keyword.ReferenceId;

                            Match regex = Regex.Match(strIngredientText, strRegex, RegexOptions.IgnoreCase);
                            string strMatchIngredients = Convert.ToString(regex);
                            if (!string.IsNullOrEmpty(strMatchIngredients))
                            {
                                string Quantity = ParseQuantity(strMatchIngredients.ToLower().Replace(keyword.Keyword.ToLower(), "").Trim());
                                //Change Start
                                //Added by ABP on 20-10-2020 regarding https://trello.com/c/O2SYZTUq/17-fractions-not-importing-correctly
                                if (!string.IsNullOrEmpty(Quantity))
                                {
                                    Match regFractions = Regex.Match(Quantity, regexFraction, RegexOptions.IgnoreCase);
                                    string matchFraction = Convert.ToString(regFractions);
                                    if (!string.IsNullOrEmpty(matchFraction))
                                    {
                                        long nResult, dResult;
                                        string[] fractions = Quantity.Split('/');
                                        if (long.TryParse(fractions[0], out nResult) && long.TryParse(fractions[1], out dResult))
                                        {
                                            if (nResult > dResult)
                                            {
                                                string firstdigit = nResult.ToString().Substring(0, 1);
                                                Quantity = firstdigit + " " + nResult.ToString().Substring(1, 1) + '/' + dResult;

                                                decimal val = FractionToDouble(Quantity);
                                                Quantity = val.ToString();
                                            }
                                        }

                                    }
                                }
                                //Change End

                                //Change Start
                                //Added By ABP on 15-10-2020
                                // Trello Link - https://trello.com/c/ZXntgjIZ/20-uom-to-read-a-as-1 
                                if (string.IsNullOrEmpty(Quantity))
                                {
                                    if (strIngredientText.Trim().StartsWith("A"))
                                    {
                                        IngredientModel unitModel = new IngredientModel
                                        {
                                            Id = pcsUnit.Id,
                                            Name = pcsUnit.Name,
                                            RowId = i,
                                            Quantity = "1",
                                            Index = regex.Index,// Get text index for ordering
                                            ReferenceId = referenceid

                                        };
                                        unitModels.Add(unitModel);
                                    }

                                }
                                //Change End
                                else if (!string.IsNullOrEmpty(Quantity))
                                {
                                    IngredientModel unitModel = new IngredientModel
                                    {
                                        Id = pcsUnit.Id,
                                        Name = pcsUnit.Name,
                                        RowId = i,
                                        Quantity = Quantity,
                                        Index = regex.Index,// Get text index for ordering
                                        ReferenceId = referenceid

                                    };
                                    unitModels.Add(unitModel);
                                }
                            }
                        }
                    }


                    int index = 99999;//For keep last when we display and save
                    foreach (var keyword in keywords)
                    {
                        IngredientModel unitModel = new IngredientModel
                        {
                            RowId = i,
                            Content = keyword.Keyword,
                            KeywordId = keyword.KeywordId,
                            Index = index,
                            IsOptional = keyword.IsOptional,
                            ReferenceId = keyword.ReferenceId
                        };
                        unitModels.Add(unitModel);
                        index++;

                    }
                }
            }
            unitModels = unitModels.OrderBy(x => x.RowId).ThenBy(x => x.Index).ToList();// Save ingredient same as display in pdf.
            return unitModels;
        }
        private string ParseQuantity(string qty)
        {
            string returnValue = string.Empty;
            if (!string.IsNullOrEmpty(qty))
            {
                foreach (char single in qty)
                {
                    if (single > 58)
                    {
                        SpecialChar specialChar = specialChars.FirstOrDefault(x => x.DefaultValue == single);
                        if (specialChar != null)
                        {
                            if (!string.IsNullOrEmpty(returnValue))
                            {
                                if (returnValue.Contains("–"))
                                {
                                    var splited = returnValue.Split('–');
                                    string temp = string.Empty;
                                    decimal quantity = 0;
                                    if (!string.IsNullOrEmpty(splited.Last()))
                                    {
                                        if (decimal.TryParse(splited.Last(), out quantity))
                                            temp = quantity.ToString() + specialChar.FractionValue;
                                    }
                                    else
                                        temp = specialChar.ReplaceValue;
                                    returnValue = splited.First() + "-" + temp;
                                }
                                else
                                {
                                    decimal quantity = 0;
                                    if (decimal.TryParse(returnValue, out quantity))
                                        returnValue = (quantity + specialChar.FractionValue).ToString();

                                }

                            }
                            else
                            {
                                returnValue = specialChar.ReplaceValue;
                            }
                        }
                        else
                        {
                            returnValue += single;
                        }
                    }
                    else
                    {
                        returnValue += single;
                    }
                }
                if (returnValue.Trim().StartsWith("."))
                {
                    returnValue = returnValue.Replace(".", "").Trim();
                }
                if (returnValue.Trim().StartsWith("·"))
                {
                    returnValue = returnValue.Replace("·", "").Trim();
                }

                if (returnValue.StartsWith("—") || returnValue.EndsWith("—"))
                {
                    Regex regex = new Regex("^—+|—+$");
                    returnValue = regex.Replace(returnValue, "");
                }
                if (returnValue.StartsWith("-") || returnValue.EndsWith("-"))
                {
                    Regex regex = new Regex("^-+|-+$");
                    returnValue = regex.Replace(returnValue, "");
                }
                if (returnValue.StartsWith("–") || returnValue.EndsWith("–"))
                {
                    Regex regex = new Regex("^–+|–+$");
                    returnValue = regex.Replace(returnValue, "");
                }

                returnValue = returnValue.Trim().Replace(" ", "-").Replace("–", "-").Replace("--", "-").Replace("1/₃", "1/3"); // Added check for 1/3 due issue mentioned in https://trello.com/c/813ZcalY/14-font-for-fractions-different
                returnValue = returnValue.Trim().Replace("--", "-");
            }
            return returnValue;
        }

        // Added By ABP on 20-10-2020
        // Static method for calculating mixed fraction 
        // Trello - https://trello.com/c/O2SYZTUq/17-fractions-not-importing-correctly
        public static decimal FractionToDouble(string fraction)
        {
            decimal result;

            if (decimal.TryParse(fraction, out result))
            {
                return result;
            }

            string[] split = fraction.Split(new char[] { ' ', '/' });

            if (split.Length == 2 || split.Length == 3)
            {
                decimal a, b;

                if (decimal.TryParse(split[0], out a) && decimal.TryParse(split[1], out b))
                {
                    if (split.Length == 2)
                    {
                        return Math.Round((decimal)b / a, 1);
                    }

                    int c;

                    if (int.TryParse(split[2], out c))
                    {
                        return Math.Round(a + (decimal)b / c, 1);
                    }
                }
            }

            throw new FormatException("Not a valid fraction.");
        }
    }

    public class SpecialChar
    {
        public int AsciiValue { get; set; }
        public char DefaultValue { get; set; }
        public string ReplaceValue { get; set; }
        public decimal FractionValue { get; set; }

        public List<SpecialChar> Intialize()
        {
            return new List<SpecialChar>
            {
                new SpecialChar() { DefaultValue = '½', FractionValue = 0.5m, ReplaceValue = "1/2" },
                new SpecialChar() { DefaultValue = '⅓', FractionValue = 0.33m, ReplaceValue = "1/3" },
                new SpecialChar() { DefaultValue = '¼', FractionValue = 0.25m, ReplaceValue = "1/4" },
                new SpecialChar() { DefaultValue = '⅕', FractionValue = 0.2m, ReplaceValue = "1/5" },
                new SpecialChar() { DefaultValue = '⅙', FractionValue = 0.16m, ReplaceValue = "1/6" },
                new SpecialChar() { DefaultValue = '⅔', FractionValue = 0.33m, ReplaceValue = "2/3" },
                new SpecialChar() { DefaultValue = '⅖', FractionValue = 0.4m, ReplaceValue = "2/5" },
                new SpecialChar() { DefaultValue = '¾', FractionValue = 0.75m, ReplaceValue = "3/4" },
                new SpecialChar() { DefaultValue = '⅗', FractionValue = 0.6m, ReplaceValue = "3/5" },
                new SpecialChar() { DefaultValue = '⅘', FractionValue = 0.8m, ReplaceValue = "4/5" },
                new SpecialChar() { DefaultValue = '⅚', FractionValue = 0.83m, ReplaceValue = "5/6" },
                new SpecialChar() { DefaultValue = '⅛', FractionValue = 0.125m, ReplaceValue = "1/8" }
            };
        }
    }


}