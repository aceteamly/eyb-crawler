﻿using EYBCore;
using EYBModel;
using EYBModel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBDal
{
    public class IngredientApproveDal
    {
        public List<IngredientApproveModel> GetAllPendingIngredientApproval(string bookName)
        {
            using (DbManager dbManager = new DbManager())
            {
                var parameter = new DbParameter().Parameters;
                if (!string.IsNullOrEmpty(bookName))
                    parameter.Add("BookName", bookName);
                var result = dbManager.ExecuteSp<IngredientApproveModel>("GetAllPendingIngredientApproval", parameter);
                return result.ToList();
            }
        }

        public List<KeywordModel> SearchMainIngredient(string keyword)
        {
            using (DbManager dbManager = new DbManager())
            {
                var parameter = new DbParameter().Parameters;
                parameter.Add("Keyword", keyword);
                var result = dbManager.ExecuteSp<KeywordModel>("SearchMainIngredient", parameter);
                return result.ToList();
            }
        }

        public Tuple<List<Select2Model>, List<Select2Model>> GetAllGroceryAndIngredientType()
        {
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSpMultiple("GetAllGroceryAndIngredientType");
                List<Select2Model> groceryType = result.Read<Select2Model>().ToList();
                List<Select2Model> ingredientTypes = result.Read<Select2Model>().ToList();
                return Tuple.Create(groceryType, ingredientTypes);
            }
        }

        public int Update_Keyword(KeywordModel model, int userId)
        {
            using (DbManager dbManager = new DbManager())
            {
                var parameter = new DbParameter().Parameters;
                parameter.Add("Id", model.Id);
                parameter.Add("Keyword", model.Keyword);
                if (!string.IsNullOrEmpty(model.ShortCode))
                    parameter.Add("ShortCode", model.ShortCode);
                if (model.GroceryTypeId != 0)
                    parameter.Add("GroceryTypeId", model.GroceryTypeId);
                if (model.IngredientTypeId != 0)
                    parameter.Add("IngredientTypeId", model.IngredientTypeId);
                if (model.ReferenceId != 0)
                    parameter.Add("ReferenceId", model.ReferenceId);
                parameter.Add("UserId", userId);
                var result = dbManager.ExecuteSp<ReturnModel>("Update_Keyword", parameter);
                return result.ToList().FirstOrDefault().Id;
            }
        }

        public int GetIngredientApproveCount()
        {
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<ReturnModel>("GetIngredientApproveCount");
                return result.ToList().FirstOrDefault().Id;
            }
        }
    }
}
