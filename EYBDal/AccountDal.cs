﻿using EYBCore;
using EYBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBDal
{
   public  class AccountDal
    {
        #region Login

        public LoginModel CheckLoginUser(String UserName, string Password)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("UserName", UserName);
            parameter.Add("Password", Password);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<EYBModel.LoginModel>("Sp_CheckLoginUser", parameter);
                return result.FirstOrDefault();
            }
        }

        #endregion
    }
}
