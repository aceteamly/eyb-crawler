﻿using Dapper;
using EYBCore;
using EYBModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBDal
{
    public class CrawlerDal
    {
        public List<CrawlableSites> Get()
        {          

            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.Query<CrawlableSites>("select cf.* , b.CrawlerUrl, b.MainTitle as 'WebsiteName'  from CrawlableSiteConfiguration " +
                "cf inner join books b on b.bookid = cf.bookid  where cf.active = 1");

                return result.ToList();
            }
        }

        public CrawlableSites GetById(int id)
        {
            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.Query<CrawlableSites>("select cf.* , b.CrawlerUrl, b.MainTitle as 'WebsiteName'  from CrawlableSiteConfiguration " +
                "cf inner join books b on b.bookid = cf.bookid  where cf.active = 1 and cf.bookid = "+id);

                return result.FirstOrDefault();
            }
        }

        public int CreateRecipe(string RecipeTitle, string RecipeLink, string ServingSize, string PhotoUrl, int bookid, string RecipeVideoUrl,string photoCredit, DateTime? DatePublished)
        {
            int recipeId = 0;
            bool isCrawlerCraweler = true;
            var parameter = new DbParameter().Parameters;
            parameter.Add("@RecipeTitle", RecipeTitle);
            parameter.Add("@RecipeLink", RecipeLink);
            parameter.Add("@ServingSize", ServingSize);
            parameter.Add("@PhotoUrl", PhotoUrl);
            parameter.Add("@bookid", bookid);
            parameter.Add("@RecipeVideoUrl", RecipeVideoUrl);
            parameter.Add("@IsCrawlerCrawled", isCrawlerCraweler);
            parameter.Add("@PhotoCredit", photoCredit);
            parameter.Add("@DatePublished", DatePublished);
            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                recipeId = dbManager.ExecuteSp<int>("CreateRecipe", parameter).FirstOrDefault();                
            }
            return recipeId;
        }

        public int InsertIngredients(DataTable _ingredientsObj , int recipeId )
        {            
            var parameter = new DbParameter().Parameters;
            parameter.Add("@Ingredients", _ingredientsObj.AsTableValuedParameter());
            parameter.Add("@RecipeId", recipeId);
            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                recipeId = dbManager.ExecuteSp<int>("sp_InsertIngredients_Crawler", parameter).FirstOrDefault();
            }
            return recipeId;
        }

        public void UpdateCrawlableSiteConfiguration(string LastCrawledRecipeName, string LastCrawledRecipesCount, string LastCrawledPageUrl, int bookid)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("@LastCrawledRecipeName", LastCrawledRecipeName);
            parameter.Add("@LastCrawledRecipesCount", LastCrawledRecipesCount);
            parameter.Add("@LastCrawledPageUrl", LastCrawledPageUrl);
            parameter.Add("@bookid", bookid);
            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.ExecuteSp<string>("sp_UpdateCrawlableSiteConfiguration", parameter);
            }
        }

        public void InsertCrawlerRunStatus(int Id, bool IsRunSuccess, string SchedulerType)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("@IsRunSuccess", IsRunSuccess);
            parameter.Add("@Id", Id);
            parameter.Add("@SchedulerType", SchedulerType);
            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.ExecuteSp<string>("sp_InsertCrawlerRunStatus", parameter);
            }
        }

        public void GetAndInsertLatestIngredients()
        {            
            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.ExecuteSp<string>("sp_GetAndInsertLatestIngredients");
            }
        }

        public void UpdateudetectedINgredients(string undetectedIngredients , int bookid)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("@undetectedIngredients", undetectedIngredients);
            parameter.Add("@recipeId", bookid);

            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.ExecuteSp<string>("SP_UpdateUndetectedIngredients", parameter);
            }

        }

        public bool IfUrlAlreadyCrawled(string url, int bookid)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("@url", url);
            parameter.Add("@bookId", bookid);
            bool result = false;

            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                result = dbManager.ExecuteSp<bool>("sp_CheckUrlAlreadyCrawled", parameter).FirstOrDefault();
            }

            return result;
        }

        public void LogError(int CrawlerId, string BookName, string RecipeName, string RecipeUrl, string ErrorDescription, string SchedulerType)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("@CrawlerId", CrawlerId);
            parameter.Add("@BookName", BookName);
            parameter.Add("@RecipeName", RecipeName);
            parameter.Add("@RecipeUrl", RecipeUrl);
            parameter.Add("@ErrorDescription", ErrorDescription);
            parameter.Add("@SchedulerName", SchedulerType);

            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.ExecuteSp<string>("sp_LogError", parameter);
            }

        }

        public void updateVideoUrl(string url, string videourl)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("@url", url);
            parameter.Add("@videourl", videourl);

            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.ExecuteSp<string>("updatevideo", parameter);
            }
        }

        public void SyncIngredients()
        {
            var parameter = new DbParameter().Parameters;
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<int>("sp_GetAndInsertIngredientData", parameter);
            }
        }

        public void SyncIngredientVariations()
        {
            var parameter = new DbParameter().Parameters;
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<int>("sp_GetAndInsertIngredientVariationsData", parameter);
            }
        }

        public List<UpdateIngredientsViewModel> GetAllOnlineUrl(int id)
        {
            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.Query<UpdateIngredientsViewModel>("select ID,OnlineUrl from recipes WITH (NOLOCK) where OnlineUrl is not null and bookid = " + id);
                return result.ToList();
            }
        }

        public List<string> GetIngredientsByRecipeId(int id)
        {
            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.Query<string>("select ig.id from RecpieIngredients ri WITH (NOLOCK) " +
                    " inner join ingredients ig on ri.ingredient = ig.ID where ri.Recipe = " + id);
                return result.ToList();
            }
        }

        public int GetMaxSortOrderRecipeIngredients(int id)
        {
            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.Query("select max(sortorder) from RecpieIngredients WITH (NOLOCK) where Recipe = " + id);
                return result;
            }
        }

        public List<string> GetExcludedIngredientsList()
        {
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.Query<string>("select ingredientname from IngredientExcludeCrawler");
                return result.ToList();
            }
        }

        public List<UpdateIngredientsViewModel> GetAllAlreadyCrawledRecipes (int id)
        {
            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.Query<UpdateIngredientsViewModel>("select ID,OnlineUrl from recipes WITH (NOLOCK) where OnlineUrl is not null and active = 1 and isapproved = 1 and bookid = " + id);
                return result.ToList();
            }
        }

        public void deleteAlreadyCrawlewdIngredients(int recipeid)
        {
            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.Query("delete from RecpieIngredients where Recipe = " + recipeid);
                dbManager.Query("update recipes set isapproved = 0  where Id = " + recipeid);
            }
        }

    }
}
