﻿using EYBCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EYBModel;
using EYBModel.Helper;
using System.Data;
using Dapper;

namespace EYBDal
{
    public class IndexDal
    {

        public List<KeywordModel> GetIngredientsKeyword(DataTable dtIngredientText)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("IngredientType", dtIngredientText.AsTableValuedParameter());
            using (DbManager dbManager = new DbManager())
            {
                
                //var result = dbManager.ExecuteSp<KeywordModel>("sp_GetIngredientKeyword", parameter); sp_GetMatchedIngredient
                var result = dbManager.ExecuteSp<KeywordModel>("sp_GetMatchingIngredientsID", parameter); 
                return result.ToList();
            }
        }
    }
}
