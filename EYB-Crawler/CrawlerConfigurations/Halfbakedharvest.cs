﻿using AngleSharp;
using AngleSharp.Dom;
using EYB_Crawler.HelperMethods;
using EYBDal;
using EYBModel;
using EYBWeb.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace EYB_Crawler.CrawlerConfigurations
{
    public class Halfbakedharvest
    {
        public readonly IConfiguration _config;
        public readonly IBrowsingContext _context;

        public Halfbakedharvest(IConfiguration config, IBrowsingContext context)
        {
            _config = config;
            _context = context;
        }

        public async Task<string> ScrapSite(CrawlableSites _obj, int ID, string url = null)
        {
            IDocument mainDocument;
            string lastCrawledPageUrl = string.Empty;
            string crawlStatus = string.Empty;

            // Before launch. We will pass each page url to get the recipes from the page.
            // then index page will be used to get the latest recipe. This shall be done only for the sites which are paging based.

            // Below conition will be executed if we pass variables from browser
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrWhiteSpace(url) && ID != 0)
            {
                mainDocument = await ReturnAngleSharpDocumentAsync(url);
                lastCrawledPageUrl = url;
            }
            // this will always be executed when crawler is crawled
            else
            {
                mainDocument = await ReturnAngleSharpDocumentAsync(_obj.CrawlerUrl);
                lastCrawledPageUrl = _obj.CrawlerUrl;

            }

            if (mainDocument != null)
            {
                var PageRecipeLinks = mainDocument.QuerySelectorAll("div.post-summary__image").SelectMany(e => e.Children).Where(f => f.HasAttribute("href")).ToList();

                int TotalPageRecipes = 0;
                string LastCrawledRecipeName = string.Empty;
                bool isCrawlSuccess = false;
                foreach (var value in PageRecipeLinks)
                {
                    string recipeTitle = string.Empty;
                    string recipeLink = string.Empty;

                    try
                    {
                        #region Variable Declarations
                        recipeTitle = string.Empty;
                        recipeLink = value.Attributes["href"].Value; ;
                        string servingSize = string.Empty;
                        string photoUrl = string.Empty;
                        string recipeVideoUrl = string.Empty;
                        isCrawlSuccess = false;
                        List<string> ingredients = new List<string>();
                        List<string> detectedIngredients = new List<string>();
                        List<string> unDetectedIngredients = new List<string>();
                        var recipeIngredientsData = new DataTable();
                        string undetectedIngredients = string.Empty;
                        DateTime? DatePublished = null;
                        DateTime DatePublished2;
                        #endregion

                        bool status = new CrawlerDal().IfUrlAlreadyCrawled(recipeLink, ID);
                        if (!status)
                        {
                            var RecipeDocument = await _context.OpenAsync(recipeLink);

                            #region Title Identifier
                            if (RecipeDocument.QuerySelector(_obj.TitleIdentifier) != null)
                            {
                                recipeTitle = RecipeDocument.QuerySelector(_obj.TitleIdentifier).TextContent;
                                recipeTitle = StringManipulation.RemoveSpaces(recipeTitle);
                                recipeTitle = StringManipulation.StripHTML(recipeTitle);
                                //recipeTitle = StringManipulation.TitleCase(recipeTitle);
                                LastCrawledRecipeName = recipeTitle;
                            }
                            else
                            {
                                recipeTitle = null;
                            }
                            #endregion

                            #region Serving Identifier
                            if (RecipeDocument.QuerySelector(_obj.ServingSizeIdentifier) != null)
                            {
                                servingSize = RecipeDocument.QuerySelector(_obj.ServingSizeIdentifier).TextContent;
                                servingSize = StringManipulation.RemoveSpaces(servingSize);
                                servingSize = StringManipulation.StripHTML(servingSize);
                            }
                            else
                            {
                                servingSize = null;
                            }
                            #endregion

                            #region Image Identifier
                            var ImageObject = value.Children.Where(e => e.TagName == "IMG").FirstOrDefault();
                            if (ImageObject != null)
                            {                                                            
                                if (ImageObject.HasAttribute("data-lazy-src"))
                                {
                                    string urlToCheck = string.Empty;
                                    urlToCheck = ImageObject.Attributes["data-lazy-src"].Value;                                   

                                    if (UriMethods.CheckUrlFormat(urlToCheck))
                                    {
                                        photoUrl = urlToCheck;
                                    }
                                    else
                                    {
                                        photoUrl = null;
                                    }
                                }
                                else
                                {
                                    photoUrl = null;
                                }

                            }
                            else
                            {
                                photoUrl = null;
                            }
                            #endregion

                            #region DatePublished Identifier
                            if (RecipeDocument.QuerySelector(".page-header__content-entry-date") != null)
                            {
                                if (RecipeDocument.QuerySelector(".page-header__content-entry-date").FirstChild.NodeName == "A")
                                {
                                    string urlToCheck = string.Empty;
                                    urlToCheck = RecipeDocument.QuerySelector(".page-header__content-entry-date").FirstChild.TextContent;
                                    if (DateTime.TryParse(urlToCheck, out DatePublished2))
                                    {
                                        DatePublished = DatePublished2;
                                    }
                                    
                                }
                                else
                                {
                                    photoUrl = null;
                                }

                            }
                            else
                            {
                                photoUrl = null;
                            }
                            #endregion

                            int recipeId = 0;
                            if (!string.IsNullOrEmpty(recipeTitle))
                            {
                                recipeId = new CrawlerDal().CreateRecipe(recipeTitle, recipeLink, servingSize, photoUrl, _obj.BookId, recipeVideoUrl, null, DatePublished); // Recipe is created here
                            }

                            if (recipeId != 0) // In case there is an error in recipe creation, the ingredients will not be inserted.
                            {
                                #region Ingredient Identifier
                                if (RecipeDocument.QuerySelector(_obj.IngredientIdentifier) != null)
                                {
                                    var list = RecipeDocument.QuerySelectorAll(_obj.IngredientIdentifier);
                                    ingredients = returnIngredientsList(list).Distinct().ToList();  // Remove all the duplicate values.

                                    List<IngredientModel> ingredientslist = new Ingredient().GetIngredientsKeyword(ingredients); // Main method to parse the ingredients
                                    var rowGroupedIngredients = ingredientslist.GroupBy(r => r.RowId);

                                    recipeIngredientsData.Columns.Add("DisplayQuantity");
                                    recipeIngredientsData.Columns.Add("Quantity");
                                    recipeIngredientsData.Columns.Add("UnitId");
                                    recipeIngredientsData.Columns.Add("IngredientId");
                                    recipeIngredientsData.Columns.Add("ReferenceId");
                                    recipeIngredientsData.Columns.Add("SortOrder");
                                    recipeIngredientsData.Columns.Add("RecipeId");

                                    int i = 1;

                                    foreach (var data in rowGroupedIngredients)
                                    {
                                        int ingredientId = 0;
                                        string quantity = string.Empty;
                                        string displayQuantity = string.Empty;
                                        int? referenceId = null;
                                        int? unitId = null;
                                        List<int> _detectedId = new List<int>();

                                        foreach (var subData in data)
                                        {
                                            if (subData.KeywordId != 0)
                                            {
                                                _detectedId.Add(subData.KeywordId);
                                                //ingredientId = subData.KeywordId;
                                            }
                                            if (subData.Id != 0 && subData.Id != -1)
                                            {
                                                unitId = subData.Id;
                                            }
                                            if (!string.IsNullOrEmpty(subData.Quantity) && !string.IsNullOrWhiteSpace(subData.Quantity))
                                            {
                                                displayQuantity = subData.Quantity;
                                                quantity = subData.Quantity;
                                            }

                                            if (subData.ReferenceId != 0)
                                            {
                                                referenceId = subData.ReferenceId;
                                            }

                                            if (!string.IsNullOrEmpty(subData.Content))
                                            {
                                                detectedIngredients.Add(subData.Content);
                                            }
                                        }

                                        ingredientId = _detectedId.FirstOrDefault();

                                        if (!recipeIngredientsData.AsEnumerable().Any(row => ingredientId.ToString() == row.Field<string>("IngredientId")) && ingredientId != 0)
                                        {
                                            recipeIngredientsData.Rows.Add(displayQuantity, quantity, unitId, ingredientId, referenceId, i, recipeId);
                                            i = i + 9;
                                        }

                                    }
                                    #region detect undetected ingredients
                                    foreach (var ingredient in detectedIngredients)
                                    {
                                        string result = ingredients.FirstOrDefault(s => s.ToLower().Contains(ingredient.ToLower()));
                                        if (!string.IsNullOrEmpty(result) && !string.IsNullOrWhiteSpace(result))
                                        {
                                            ingredients.Remove(result);
                                        }
                                    }

                                    foreach (var undetectedIngredient in ingredients)
                                    {
                                        undetectedIngredients += undetectedIngredient + "; ";
                                    }
                                    #endregion

                                    if (recipeIngredientsData != null)
                                    {
                                        if (recipeIngredientsData.Rows.Count > 0)
                                        {
                                            int success = new CrawlerDal().InsertIngredients(recipeIngredientsData, recipeId);
                                        }
                                    }
                                    if (!string.IsNullOrWhiteSpace(undetectedIngredients) && !string.IsNullOrEmpty(undetectedIngredients))
                                    {
                                        new CrawlerDal().UpdateudetectedINgredients(undetectedIngredients, recipeId);
                                    }
                                }
                                #endregion


                                isCrawlSuccess = true;
                                TotalPageRecipes++;
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        //Log.Error(ex.ToString());
                        new CrawlerDal().LogError(_obj.Id, _obj.WebsiteName, recipeTitle, recipeLink, ex.ToString(), "HalfBaked Scrap Data");
                        crawlStatus = "Error in crawling. Please check url povided or check error logs";
                        isCrawlSuccess = false;
                        //return "Error in crawling. Please check url povided or check error logs";
                    }

                }

                if (isCrawlSuccess)
                {
                    new CrawlerDal().InsertCrawlerRunStatus(_obj.Id, true, "HalfBaked Scrap Data");
                    new CrawlerDal().UpdateCrawlableSiteConfiguration(LastCrawledRecipeName, Convert.ToString(TotalPageRecipes), lastCrawledPageUrl, _obj.BookId);
                    crawlStatus = "Your url has been crawled successfully. Please check main site to see changes";
                }
                else
                {
                    if (PageRecipeLinks.Count == 0)
                    {
                        new CrawlerDal().LogError(_obj.Id, _obj.WebsiteName, "", "", "No Recipe Link Found on Index Page.", "HalfBaked Scrap Data");
                    }
                    new CrawlerDal().InsertCrawlerRunStatus(_obj.Id, false, "HalfBaked Scrap Data");
                }


                return crawlStatus;
            }
            else
            {
                new CrawlerDal().InsertCrawlerRunStatus(_obj.Id, false, "HalfBaked Harvest ");
                new CrawlerDal().LogError(_obj.Id, _obj.WebsiteName, "", "", "Main Page Crawled Failed", "HalfBaked Scrap Data");
                return "Error in crawling. Please check url povided or check error logs";
            }

        }

        #region method returning AngleSharp Document
        public async Task<IDocument> ReturnAngleSharpDocumentAsync(string url)
        {
            IDocument _iDocument;

            _iDocument = await _context.OpenAsync(url);

            return _iDocument;
        }
        #endregion

        #region Return Ingredient List
        public List<string> returnIngredientsList(dynamic _obj)
        {
            List<string> ingredients = new List<string>();

            foreach (var value1 in _obj)
            {
                string ingredienttext = string.Empty;
                foreach (var children in value1.Children)
                {
                    ingredienttext += children.TextContent + " ";
                }
                ingredienttext = StringManipulation.RemoveSpaces(value1.TextContent);
                ingredienttext = StringManipulation.StripHTML(ingredienttext);
                ingredients.Add(ingredienttext);

            }

            return ingredients;
        }
        #endregion
    }

}