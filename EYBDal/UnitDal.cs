﻿using EYBCore;
using EYBModel;
using EYBModel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBDal
{
    public class UnitDal
    {
        public int SaveUnit(UnitModel unitModel)
        {
            string spName = unitModel.Id == 0 ? "Insert_Unit" : "Update_Unit";
            var parameter = new DbParameter().Parameters;

            if (unitModel.Id != 0)
            {
                parameter.Add("Id", unitModel.Id);
            }
            parameter.Add("Name", unitModel.Name);
            if(unitModel.BaseId!=0)
            parameter.Add("BaseId", unitModel.BaseId);
            if (unitModel.RelatedId != 0)
                parameter.Add("RelatedId", unitModel.RelatedId);
            if (unitModel.RelatedRate.HasValue && unitModel.RelatedRate.Value!=0)
                parameter.Add("RelatedRate", unitModel.RelatedRate.Value);
            parameter.Add("UserId", unitModel.UserId);

            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<ReturnModel>(spName, parameter);
                return result.FirstOrDefault().Id;
            }
        }

        public UnitModel Get_Unit(int id)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("Id", id);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<UnitModel>("Select_Unit", parameter);
                return result.FirstOrDefault();
            }
        }

        public int ActiveInActiveUnit(int id,bool isActive, int userId)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("Id", id);
            parameter.Add("IsActive", isActive);
            parameter.Add("UserId", userId);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<ReturnModel>("ActiveInActiveUnit", parameter);
                return result.FirstOrDefault().Id;
            }
        }

        public List<UnitModel> GetUnits(bool isAll, bool? isActive)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("IsAll", isAll);
            UnitModel _obj = new UnitModel();
            _obj.Id = -1;
            _obj.Name = "";
            if (isActive.HasValue)
                parameter.Add("IsActive", isActive);
            using (DbManagerEYBSource dbManager = new DbManagerEYBSource())
            {
                var result = dbManager.ExecuteSp<UnitModel>("sp_GetUnits");
                var result2 = result.ToList();
                result2.Add(_obj);
                return result2;
            }


        }
    }
}
