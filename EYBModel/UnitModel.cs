﻿using EYBModel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    public class UnitModel : DbBaseModel
    {
        public new int Id { get; set; }
        public string Name { get; set; }
        public int BaseId { get; set; }
        public int RelatedId { get; set; }
        public double? RelatedRate { get; set; }
        public int RowId { get; set; }
        public int UserId { get; set; }
        public string BaseName { get; set; }
        public string RelatedName { get; set; }
        public string ModifiedByName { get; set; }
        public bool IsActive { get; set; }
        public List<UnitModel> BaseUnits { get; set; }

        public UnitModel()
        {
            BaseUnits = new List<UnitModel>();
        }
    }

}
