﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    public class CrawlableSites
    {
        public virtual int Id {get;set;}
        public virtual int BookId {get;set;}
        public virtual string TitleIdentifier {get;set;}
        public virtual string IngredientIdentifier {get;set;}
        public virtual string ServingSizeIdentifier {get;set;}
        public virtual string PhotosIdentifier {get;set;}
        public virtual string VideoIdentifier {get;set;}
        public virtual DateTime? LastRunDate {get;set;}
        public virtual string LastCrawledRecipeName {get;set;}
        public virtual string LastCrawledRecipesCount {get;set;}
        public virtual string CrawlerUrl {get; set; }
        public virtual string WebsiteName { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual int CreateBy { get; set; }
        public virtual DateTime? ModifiedDate { get; set; }
        public virtual int ModifiedBy { get; set; }
        public virtual bool Active { get; set; }

        public virtual string LastCrawledPageUrl { get; set; }
    }
}
