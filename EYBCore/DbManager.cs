﻿using Dapper;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using static Dapper.SqlMapper;

namespace EYBCore
{
    public class DbManager : System.IDisposable
    {
        SqlConnection connection;

        #region Constructor

        public DbManager()
        {
            Connect();
        }

        #endregion

        #region Private Methods

        private void Connect()
        {
            connection = new SqlConnection();
            string conncetionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            connection = new SqlConnection(conncetionString);
            connection.Open();
        }

        #endregion

        #region Database specific methods

        public IEnumerable<T> ExecuteSp<T>(string spName)
        {
            var result = connection.Query<T>(spName, commandType: CommandType.StoredProcedure, commandTimeout: 600);
            return result;
        }

        public IEnumerable<T> ExecuteSp<T>(string spName, Dapper.DynamicParameters parameters)
        {
            var result = connection.Query<T>(spName, parameters, commandType: CommandType.StoredProcedure, commandTimeout: 600);
            return result;
        }

        public GridReader ExecuteSpMultiple(string spName, Dapper.DynamicParameters parameters)
        {
            var result = connection.QueryMultiple(spName, parameters, commandType: CommandType.StoredProcedure);
            return result;
        }
        public GridReader ExecuteSpMultiple(string spName)
        {
            var result = connection.QueryMultiple(spName, commandType: CommandType.StoredProcedure);
            return result;
        }

        public IEnumerable<T> ExecuteSp<T>(string spName, ref Dapper.DynamicParameters parameters)
        {
            var result = connection.Query<T>(spName, parameters, commandType: CommandType.StoredProcedure, commandTimeout: 600);
            return result;
        }
        public IEnumerable<T> Query<T>(string query)
        {
            var result = connection.Query<T>(query);
            return result;
        }
        #endregion

        #region Interface Methods

        public void Dispose()
        {
            if (connection != null)
            {
                connection.Close();
                connection.Dispose();
            }
            connection = null;
        }

        #endregion

    }
}

