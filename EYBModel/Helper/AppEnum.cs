﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel.Helper
{
    public enum enmHtmlProcessStatus
    {
        Procesessing = 1,
        IndexPending = 2,
        IndexCompleted = 3,
        Deleted = 4,
        ReadyForTransfter = 5,
        TransferInprogress = 6
    }

    public enum AssignmentType
    {
        Automated = 1,
        Manual,
        Proofreading
    }
}
