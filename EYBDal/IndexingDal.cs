﻿using EYBCore;
using EYBModel;
using EYBModel.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EYBDal
{
    public class IndexingDal
    {
        public DataTable GetIngredientsKeyword(DataTable dtIngredientText)
        {
            DataTable dt = new DataTable();

            try
            {
                var parameter = new DbParameter().Parameters;
                parameter.Add("IngredientType", dtIngredientText);

                SqlDataAdapter da = new SqlDataAdapter();
                using (DbManager dbManager = new DbManager())
                {
                    //da.SelectCommand = dbManager.ExecuteSp<Dt>("sp_GetIngredientKeyword", parameter);
                    da.Fill(dt);
                    return dt;
                }
              
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public  DataTable createBlankDataTable(string[,] arrIngredient)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RowID", typeof(int));
            dt.Columns.Add("IngredientText", typeof(string));

            if (arrIngredient != null && arrIngredient.Length > 0)
            {
                for (int i = 0; i < (arrIngredient.Length / 2); i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["RowID"] = Convert.ToString(Convert.ToInt32(arrIngredient[i, 0]) + 1);
                    dr["IngredientText"] = Convert.ToString(arrIngredient[i, 1]);
                    dt.Rows.Add(dr);
                }
            }

            return dt;
        }
     


    }
}
