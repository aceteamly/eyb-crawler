﻿using EYBModel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    public class IngredientApproveModel : KeywordModel
    {
        public string UserName { get; set; }
        public string BookName { get; set; }
        public int BookId { get; set; }
        public int OrderNo { get; set; }
        public int RecipeNumber { get; set; }
        public string RecipeName { get; set; }
    }
}
