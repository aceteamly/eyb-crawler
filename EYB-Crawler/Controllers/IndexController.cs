﻿using EYBDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace EYB_Crawler.Controllers
{
    public class IndexController : ApiController
    {
        // GET api/<controller>
        [HttpGet]
        public string Get()
        {
            return "EYB-Crawler Version 1.0";
            //var tuple = new IndexDal().GetDimensionByRecipeId(1, 0, false);
        }
    }
}
