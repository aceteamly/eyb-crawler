﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel.Helper
{
    public class ReturnModel
    {
        public int Id { get; set; }
        public int RecipeNumber { get; set; }
        public int RecipeCount { get; set; }
    }
}
