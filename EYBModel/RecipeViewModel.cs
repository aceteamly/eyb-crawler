﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    public class RecipeViewModel
    {
        public int PdfBookId { get; set; }
        public string Title { get; set; }
        public int TitleId { get; set; }
        public string SubTitle { get; set; }
        public int SubTitleId { get; set; }
        public string ServingSize { get; set; }
        public int ServingSizeId { get; set; }
        public int PdfBookRecipeId { get; set; }
        public List<RecipeViewDescription> Descriptions { get; set; }
        public List<IngredientViewModel> IngredientModels { get; set; }
        public Associate_Recipe_CategoriesModel Category { get; set; }
        public List<UnitModel> units { get; set; }
        public bool IsCalculationView { get; set; }
        public bool PrimaryIngredient { get; set; }
        public int PersonSize { get; set; }

        public RecipeViewModel()
        {
            IngredientModels = new List<IngredientViewModel>();
            Descriptions = new List<RecipeViewDescription>();
            Category = new Associate_Recipe_CategoriesModel();
        }
    }

    public class RecipeViewDescription
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
