﻿using EYBModel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    public class BookListModel : DbBaseModel
    {
        public string BookName { get; set; }
        public string ISBN { get; set; }
        public string AuthorName { get; set; }
        public bool IndexingStatus { get; set; }
        public int TotalRecords { get; set; }
        public int TotalPages { get; set; }
        public int ExcludedPages { get; set; }
        public string PdfName { get; set; }
        public string ThumbnailName { get; set; }
        public int ProcessStatus { get; set; }
        public string ModifieddByName { get; set; }
        public string IndexStatus { get; set; }
        public string ModifieddDate { get; set; }
        public string ContainerName { get; set; }
        public string TotalRecipe { get; set; }
        public int IsReadyForMigration { get; set; }
        public int AssignmentType { get; set; }
        public string AssignmentTypeString { get; set; }

    }
}
