﻿using EYBModel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    public class BookPdfHtmlModel : DbBaseModel
    {
        public int PdfBookId { get; set; }
        public string PageName { get; set; }
        public int OrderNo { get; set; }
        public bool IsExluded { get; set; }
        public string ContainerName { get; set; }
        public string PdfName { get; set; }
        public int TotalCount { get; set; }
        public int ExcludeCount { get; set; }
        public bool IsFlagged { get; set; }
        public string FlagDescription { get; set; }
        public int PageNumber { get; set; }
        public int DeletedBy { get; set; }
        public DateTime? DeletedDateTime { get; set; }
        public string DynamicHTMLContent { get; set; }
        public int PdfBookPageId { get; set; }
        public int RecipeNumber { get; set; }
        public int PdfBookRecipeId { get; set; }
        public int DisplayPageNumber { get; set; }
        public int ContinueRecipeCount { get; set; }
        public int NextVariantNo { get; set; }
        public int NextRecipeNumber { get; set; }
        public bool HasMultiple { get; set; }
        public bool HasVariant { get; set; }
        public int VariantBaseId { get; set; }
        public List<int> RecipeNumbers { get; set; }
        public List<int> VariantRecipeNumbers { get; set; }
        public List<PdfBookDimensionsModel> dimensionList { get; set; }
        public string AdminComment { get; set; }
        public string IndexerComment { get; set; }
        public bool? IndexerFlag { get; set; }

        public BookPdfHtmlModel()
        {
            RecipeNumbers = new List<int>();
            VariantRecipeNumbers = new List<int>();
        }
    }
}
