﻿using EYBModel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    public class BookSearchModel : DatatableModel
    {        
        public string BookName { get; set; }
        public string ISBN { get; set; }
        public string AuthorName { get; set; }
        public int ProcessingStatus { get; set; }
        public bool IndexingStatus { get; set; }
        public bool IsPending { get; set; }
        public bool IsProcessing { get; set; }
        public bool IsCompete { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
    }
}
