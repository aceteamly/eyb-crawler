﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    /// <summary>
    /// Creator - ABP
    /// Created Date - 21-09-2020
    /// </summary>
    public class IngredientsModel
    {
        public int Id { get; set; }
        public string Keyword { get; set; }
    }
}
