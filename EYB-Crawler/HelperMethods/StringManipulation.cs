﻿using EYBDal;
using EYBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace EYB_Crawler.HelperMethods
{
    public static class StringManipulation
    {
        public static string RemoveSpaces(string word)
        {
            if (!string.IsNullOrEmpty(word) && !string.IsNullOrEmpty(word))
            {
                word = Regex.Replace(word, @"\s+", " ");
                word = word.Replace(".", string.Empty);
                return word;
            }

            return word;
        }

        public static string StripHTML(string word)
        {
            if (!string.IsNullOrEmpty(word) && !string.IsNullOrWhiteSpace(word))
            {
                return Regex.Replace(word, "<.*?>", string.Empty);
            }
            return word;
        }

        public static string RemoveLetters(string word)
        {
            if (!string.IsNullOrWhiteSpace(word) && !string.IsNullOrEmpty(word))
            {
                return Regex.Replace(word, "[^0-9.]", string.Empty);
            }
            return word;
        }

        public static string TitleCase(string word)
        {
            if (!string.IsNullOrWhiteSpace(word) && !string.IsNullOrEmpty(word))
            {
                word = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(word.ToLower());
            }
            return word;
        }

        public static List<KeywordModel> RemoveExcludedIngredients(List<KeywordModel> Ingredients)
        {
            List<string> excludedIngredients = new CrawlerDal().GetExcludedIngredientsList();            
            return Ingredients.Where(b => !excludedIngredients.Any(a => b.Keyword.ToUpper().Equals(a.ToUpper()))).ToList();
        }
    }
}