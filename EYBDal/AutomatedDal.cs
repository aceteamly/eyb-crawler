﻿using EYBCore;
using EYBModel;
using EYBModel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBDal
{
    public class AutomatedDal
    {
        public List<BookPdfHtmlModel> GetAutomatedIndexPages(int pdfBookId,int pdfBookPageId, bool isDeleteAlreadyIndexed)
        {
            var parameter = new DbParameter().Parameters;
            parameter.Add("PdfBookId", pdfBookId);
            parameter.Add("PdfBookPageId", pdfBookPageId);
            parameter.Add("IsDeleteAlreadyIndexed", isDeleteAlreadyIndexed);
            using (DbManager dbManager = new DbManager())
            {
                var result = dbManager.ExecuteSp<BookPdfHtmlModel>("GetAutomatedIndexPages", parameter);
                return result.ToList();
            }
        }
    }
}
