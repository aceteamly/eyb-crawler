﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EYB_Crawler.HelperMethods
{
    public static class UriMethods
    {
        public static bool CheckUrlFormat(string url)
        {
            Uri uriResult;
            bool result = false;

            return result = Uri.TryCreate(url, UriKind.Absolute, out uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }

    }
}