﻿using EYBModel.Helper;
using System.Collections.Generic;

namespace EYBModel
{
    public class PdfBookModel: DbBaseModel
    {
        public string ContainerName { get; set; }
        public string PdfName { get; set; }
        public string ThumbnailName { get; set; }
        public int ProcessStatus { get; set; } 
        public string ISBN { get; set; }
        public bool IndexingStatus { get; set; }
    }
}
