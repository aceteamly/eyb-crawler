﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
   public class KeywordModel
    {
        public int OrderId { get; set; }
        public int LineID { get; set; }
        //public int IngredientId { get; set; }
        public int Id { get; set; }
        public string Keyword { get; set; }
        public string ShortCode { get; set; }
        public int KeywordId { get; set; }
        public int ReferenceId { get; set; }
        public int IngredientTypeId { get; set; }
        public int GroceryTypeId { get; set; }
        public bool IsOptional { get; set; }
    }
}
