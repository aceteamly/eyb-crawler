﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel
{
    public class IngredientModel : UnitModel
    {
        public string Quantity { get; set; }
        public string Content { get; set; }
        public bool IsPageNumber { get; set; }
        public int KeywordId { get; set; }
        public int Index { get; set; }
        public bool IsOptional { get; set; }
        public int ReferenceId { get; set; }
    }

    public class IngredientContentModel
    {
        public int Id { get; set; }
        public int IngredientOrderNo { get; set; }
        public bool IsManuallyAdded { get; set; }
        public int IngredientId { get; set; }
        public int OrderNo { get; set; }
        public string Content { get; set; }
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public int RowId { get; set; }
        public string Quantity { get; set; }
        public string MergeContent { get; set; }
        public bool IsPageNumber { get; set; }
        public int PdfBookPageId { get; set; }
        public int ReferenceRecipeId { get; set; }
        public int KeywordId { get; set; }
        public int VariantBaseId { get; set; }
        public int PdfBookRecipeId { get; set; }
        public bool IsOptional { get; set; }
        public double? Qty { get; set; }
        public double? Qty2 { get; set; }
        public string Content2 { get; set; }
        public int? UnitId2 { get; set; }
        public int VC { get; set; }

        public IngredientContentModel()
        {
            Content = string.Empty;
        }

    }

    public class IngredientViewModel
    {
        public int IngredientOrderNo { get; set; }
        public int RowId { get; set; }
        public int OrderNo { get; set; }
        public string MergeContent { get; set; }
        public int UnitId { get; set; }
        public int PdfBookRecipeId { get; set; }
        public int RecipeNumber { get; set; }
        public bool IsPageNumber { get; set; }
        public bool RefPdfBookRecipeId { get; set; }
        public bool IsOptional { get; set; }
        public double? Qty { get; set; }
        public double? Qty2 { get; set; }
        public string Content2 { get; set; }
        public int? UnitId2 { get; set; }
        public string MergeContent2 { get; set; }
        public string UnitName1 { get; set; }
        public string UnitName2 { get; set; }
        public double? RelatedRate { get; set; }

    }
}
