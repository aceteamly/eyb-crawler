﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBModel.Helper
{
   public class AppString_Validation
    {
        public const string Req_UserName = "Username is required";
        public const string Req_Password = "Password is required";
        public const string Invalid_UserName_Password = "Invaild username or password.";
        public const string Req_SearchISBNBookRecord = "Please search ISBN book record.";
        public const string Req_Reactangle = "Please draw at least one rectangle.";
        
    }

    public class AppString_Message
    {
        public const string SomethingWantWrong = "Something went wrong.";
        public const string FilesUpload = "File(s) uploaded successfully.";
        public const string BookDelete = "Book deleted successfully.";
        public const string BookUpdated = "Book data updated successfully.";
        public const string BookNotFoundByISBN = "No record found with the given ISBN.";
        public const string PageNotFound = "Page not found.";
        public const string RecipeSaved = "Recipe saved successfully.";
        public const string PageNumberUpdated = "Book Page Number updated successfully.";
        public const string FlagUpdated = "Flag applied successfully.";
        public const string FlagRemoved = "Flag removed successfully.";
        public const string RecordNotFound = "Record not found.";
        public const string KeywordInsert = "Keyword insert successfully.";
        public const string IngredientNotFound = "Ingredient not found.";
        public const string PageisExcluded = "Page is excluded or does not exists.";
        public const string DimensionNotFound = "Please create indexing of atleast one recipe.";
        public const string AllPageIndexed = "All pages are indexed.";
        public const string CommentUpdated = "Comment saved successfully.";
        public const string BookTransfter = "Book transfter started.";
        public const string BookUnlock = "Book unlock successfully.";
        public const string UnitSave = "Unit saved successfully.";
        public const string UnitAlreadyExists = "Unit is already exists.";
        public const string UnitRelatedAssigned = "Lowest unit is already assigned.";
        public const string UnitDelete = "Unit deleted successfully.";
    }
}
