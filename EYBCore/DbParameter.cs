﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EYBCore
{
    /// <summary>
    /// DbParameter class used to manage collection of parameter with dapper execution
    /// It contains Parameters property later on will extended this class as per need.
    /// </summary>
    public class DbParameter
    {
        public Dapper.DynamicParameters Parameters { get; set; }

        public DbParameter()
        {
            Parameters = new Dapper.DynamicParameters();
        }
    }
}
