﻿using AngleSharp;
using AngleSharp.Dom;
using EYB_Crawler.HelperMethods;
using EYBDal;
using EYBModel;
using EYBWeb.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace EYB_Crawler.CrawlerConfigurations
{
    public class Nigella
    {
        public readonly IConfiguration _config;
        public readonly IBrowsingContext _context;

        public Nigella(IConfiguration config, IBrowsingContext context)
        {
            _config = config;
            _context = context;
        }

        public async Task<string> ScrapSite(CrawlableSites _obj, int ID, string url = null)
        {
            IDocument mainDocument;
            XmlDocument xdoc = new XmlDocument();
            string lastCrawledPageUrl = string.Empty;
            string RecipeLink = string.Empty;
            string crawlStatus = string.Empty;

            // Before launch. We will pass each page url to get the recipes from the page.
            // then index page will be used to get the latest recipe.

            try
            {
                if (!string.IsNullOrEmpty(url) && !string.IsNullOrWhiteSpace(url) && ID != 0)
                {
                    mainDocument = await ReturnAngleSharpDocumentAsync(url);
                    lastCrawledPageUrl = _obj.CrawlerUrl;
                    RecipeLink = url;
                    bool isMemberorGuest = (RecipeLink.Contains("/members") || RecipeLink.Contains("/members/")); // We are not crawling member(community recipes) as specified in https://trello.com/c/nkB5lZnU/5-nigella-lawson
                    if (!isMemberorGuest)
                    {
                        bool status = new CrawlerDal().IfUrlAlreadyCrawled(RecipeLink, ID);
                        if (!status)
                        {
                            crawlStatus = await CrawlUrlAsync(RecipeLink, _obj, ID);
                        }
                        else
                        {
                            crawlStatus = "This url has already been crawled";
                        }
                    }
                }
                else
                {
                    int TotalPageRecipes = 0;
                    bool status = false;
                    RecipeLink = _obj.CrawlerUrl;
                    xdoc.Load(RecipeLink);//Loads the site map file into XML doc      
                    string LastCrawledRecipeName = string.Empty;

                    XmlNodeList xNodelst = xdoc.DocumentElement.ChildNodes;

                    var macthingNodes = xNodelst.Cast<XmlNode>().Where(n => n.FirstChild.InnerText.Contains("/recipes/") && !n.FirstChild.InnerText.Contains("/members")
                                        && !n.FirstChild.InnerText.Contains("/members/")).OrderByDescending(n => n.ChildNodes[1].InnerText);                              // We are not crawling member(community recipes) as specified in https://trello.com/c/nkB5lZnU/5-nigella-lawson

                    // Returns list of matching uls
                    foreach (var value in macthingNodes)
                    {
                        try
                        {
                            XmlNode loc = value.ChildNodes.Cast<XmlNode>().Where(n => n.Name == "loc").FirstOrDefault();
                            string recipeUrl = loc.FirstChild.Value;
                            bool isMemberorGuest = (recipeUrl.Contains("/members") || recipeUrl.Contains("/members/")); // Checking for Community Recipes
                            if (!isMemberorGuest)
                            {
                                status = new CrawlerDal().IfUrlAlreadyCrawled(recipeUrl,_obj.BookId);
                                if (!status)
                                {
                                     LastCrawledRecipeName = recipeUrl;
                                    await CrawlUrlAsync(recipeUrl, _obj, 0);
                                    TotalPageRecipes++;
                                }
                                else
                                {
                                    continue;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            new CrawlerDal().LogError(_obj.Id, _obj.WebsiteName, "", RecipeLink, ex.ToString(), "Nigella Scrap Data");
                            new CrawlerDal().InsertCrawlerRunStatus(_obj.Id, false, "Nigella Scrap Data");
                            crawlStatus = "Error in crawling. Please check url povided or check error logs";
                        }
                    }
                    if (!status)
                    {
                        new CrawlerDal().UpdateCrawlableSiteConfiguration(LastCrawledRecipeName, Convert.ToString(TotalPageRecipes.ToString()), lastCrawledPageUrl, _obj.BookId);
                    }

                    new CrawlerDal().InsertCrawlerRunStatus(_obj.Id, true, "Nigella Scrap Data");
                    crawlStatus = "Your url has been crawled successfully. Please check main site to see changes";
                    return crawlStatus;
                }

            }
            catch (Exception ex)
            {
                new CrawlerDal().LogError(_obj.Id, _obj.WebsiteName, "", RecipeLink, ex.ToString(), "Nigella Scrap Data");
                crawlStatus = "Error in crawling. Please check url povided or check error logs";
            }

            return crawlStatus;
        }

        #region method returning AngleSharp Document
        public async Task<IDocument> ReturnAngleSharpDocumentAsync(string url)
        {
            IDocument _iDocument;

            _iDocument = await _context.OpenAsync(url);

            return _iDocument;
        }
        #endregion

        #region main method for crawling

        public async Task<string> CrawlUrlAsync(string RecipeLink, CrawlableSites _obj, int Id)
        {
            string crawlerStatus = string.Empty;
            bool isCrawlSuccess = false;

            if (!string.IsNullOrEmpty(RecipeLink))
            {
                string recipeTitle = string.Empty;
                string LastCrawledRecipeName = string.Empty;
                try
                {
                    #region Variable Declarations
                    recipeTitle = string.Empty;
                    string servingSize = string.Empty;
                    string photoUrl = string.Empty;
                    string recipeVideoUrl = string.Empty;
                    List<string> ingredients = new List<string>();
                    List<string> detectedIngredients = new List<string>();
                    List<string> unDetectedIngredients = new List<string>();
                    var recipeIngredientsData = new DataTable();
                    string undetectedIngredients = string.Empty;
                    string photoCredit = string.Empty;
                    #endregion                    

                    var RecipeDocument = await _context.OpenAsync(RecipeLink);

                    if (RecipeDocument.QuerySelector(_obj.TitleIdentifier) != null)
                    {
                        if (!RecipeDocument.ToHtml().Contains("this recipe is not currently online"))
                        {
                            #region Title Identifier

                            if (RecipeDocument.QuerySelector(_obj.TitleIdentifier) != null)
                            {
                                if (RecipeDocument.QuerySelector(_obj.TitleIdentifier).HasAttribute("itemprop"))
                                {
                                    recipeTitle = RecipeDocument.QuerySelector(_obj.TitleIdentifier).TextContent;
                                    recipeTitle = StringManipulation.RemoveSpaces(recipeTitle);
                                    recipeTitle = StringManipulation.StripHTML(recipeTitle);
                                    recipeTitle = StringManipulation.RemoveSpaces(recipeTitle);
                                    LastCrawledRecipeName = recipeTitle;
                                }
                                else
                                {
                                    recipeTitle = null;
                                }
                            }
                            else
                            {
                                recipeTitle = null;
                            }
                            #endregion

                            #region Serving Identifier
                            if (RecipeDocument.QuerySelector(_obj.ServingSizeIdentifier) != null)
                            {
                                servingSize = RecipeDocument.QuerySelector(_obj.ServingSizeIdentifier).TextContent;
                                servingSize = StringManipulation.RemoveSpaces(servingSize);
                                servingSize = StringManipulation.StripHTML(servingSize);
                                //servingSize = StringManipulation.RemoveLetters(servingSize);
                            }
                            else
                            {
                                servingSize = null;
                            }
                            #endregion

                            #region Image Identifier
                            if (RecipeDocument.QuerySelector(_obj.PhotosIdentifier) != null)
                            {
                                if (RecipeDocument.QuerySelector(_obj.PhotosIdentifier).HasAttribute("src"))
                                {
                                    string urlToCheck = string.Empty;
                                    urlToCheck = ((AngleSharp.Html.Dom.IHtmlImageElement)((AngleSharp.Html.Dom.IHtmlImageElement)RecipeDocument.QuerySelector(_obj.PhotosIdentifier))).Source;
                                    if (UriMethods.CheckUrlFormat(urlToCheck))
                                    {
                                        photoUrl = urlToCheck;
                                    }
                                    else
                                    {
                                        photoUrl = null;
                                    }

                                }
                                else
                                {
                                    photoUrl = null;
                                }
                                if (RecipeDocument.QuerySelector(_obj.PhotosIdentifier).NextElementSibling.TagName.ToLower() == "figcaption")
                                {

                                    if (RecipeDocument.QuerySelector(_obj.PhotosIdentifier).NextElementSibling.FirstElementChild.TagName.ToLower() == "address")
                                    {
                                        photoCredit = RecipeDocument.QuerySelector(_obj.PhotosIdentifier).NextElementSibling.FirstElementChild.InnerHtml;
                                        photoCredit = photoCredit.Replace("Photo by ", "");
                                        photoCredit = photoCredit.Replace("Illustration by ", "");
                                        photoCredit = photoCredit.Replace("Photography by ", "");
                                        photoCredit = photoCredit.Replace("Cover illustration by ", "");

                                    }

                                }
                                else
                                {
                                    photoCredit = null;
                                }
                            }
                            else
                            {
                                photoUrl = null;
                                photoCredit = null;
                            }
                            #endregion
                            int recipeId = 0;
                            if (!string.IsNullOrEmpty(recipeTitle))
                            {
                                recipeId = new CrawlerDal().CreateRecipe(recipeTitle, RecipeLink, servingSize, photoUrl, _obj.BookId, recipeVideoUrl, photoCredit,null); // Recipe is created here
                            }

                            if (recipeId != 0) // In case there is an error in recipe creation, the ingredients will not be inserted.
                            {
                                #region Ingredient Identifier
                                if (RecipeDocument.QuerySelector(_obj.IngredientIdentifier) != null)
                                {
                                    var list = RecipeDocument.QuerySelectorAll(_obj.IngredientIdentifier);
                                    foreach (var value1 in list)
                                    {
                                        if (value1.HasAttribute("itemprop"))
                                        {
                                            if (value1.Attributes["itemprop"].Value == "recipeIngredient")
                                            {
                                                string ingredienttext = string.Empty;
                                                ingredienttext = StringManipulation.RemoveSpaces(value1.TextContent);
                                                ingredienttext = StringManipulation.StripHTML(ingredienttext);
                                                ingredients.Add(ingredienttext);
                                            }
                                        }

                                    }

                                    ingredients = ingredients.Distinct().ToList(); // Remove all the duplicate values.
                                    //ingredients = StringManipulation.RemoveExcludedIngredients(ingredients);

                                    List<IngredientModel> ingredientslist = new Ingredient().GetIngredientsKeyword(ingredients); // Main method to parse the ingredients
                                    var rowGroupedIngredients = ingredientslist.GroupBy(r => r.RowId);

                                    recipeIngredientsData.Columns.Add("DisplayQuantity");
                                    recipeIngredientsData.Columns.Add("Quantity");
                                    recipeIngredientsData.Columns.Add("UnitId");
                                    recipeIngredientsData.Columns.Add("IngredientId");
                                    recipeIngredientsData.Columns.Add("ReferenceId");
                                    recipeIngredientsData.Columns.Add("SortOrder");
                                    recipeIngredientsData.Columns.Add("RecipeId");

                                    int i = 1;

                                    foreach (var data in rowGroupedIngredients)
                                    {
                                        int ingredientId = 0;
                                        string quantity = string.Empty;
                                        string displayQuantity = string.Empty;
                                        int? referenceId = null;
                                        int? unitId = null;
                                        List<int> _detectedId = new List<int>();
                                        

                                        foreach (var subData in data)
                                        {
                                            if (subData.KeywordId != 0)
                                            {
                                                _detectedId.Add(subData.KeywordId);
                                                //ingredientId = subData.KeywordId;
                                            }
                                            if (subData.Id != 0 && subData.Id != -1)
                                            {
                                                unitId = subData.Id;
                                            }
                                            if (!string.IsNullOrEmpty(subData.Quantity) && !string.IsNullOrWhiteSpace(subData.Quantity))
                                            {
                                                displayQuantity = subData.Quantity;
                                                quantity = subData.Quantity;
                                            }

                                            if (subData.ReferenceId != 0)
                                            {
                                                referenceId = subData.ReferenceId;
                                            }

                                            if (!string.IsNullOrEmpty(subData.Content))
                                            {
                                                detectedIngredients.Add(subData.Content);
                                            }
                                        }

                                        ingredientId = _detectedId.FirstOrDefault();

                                        if (!recipeIngredientsData.AsEnumerable().Any(row => ingredientId.ToString() == row.Field<string>("IngredientId")) && ingredientId != 0)
                                        {
                                            recipeIngredientsData.Rows.Add(displayQuantity, quantity, unitId, ingredientId, referenceId, i, recipeId);
                                            i = i + 9;
                                        }

                                    }

                                    #region detect undetected ingredients
                                    foreach (var ingredient in detectedIngredients)
                                    {
                                        string result = ingredients.FirstOrDefault(s => s.ToLower().Contains(ingredient.ToLower()));
                                        if (!string.IsNullOrEmpty(result) && !string.IsNullOrWhiteSpace(result))
                                        {
                                            ingredients.Remove(result);
                                        }
                                    }

                                    foreach (var undetectedIngredient in ingredients)
                                    {
                                        undetectedIngredients += undetectedIngredient + "; ";
                                    }
                                    #endregion

                                    if (recipeIngredientsData != null)
                                    {
                                        if (recipeIngredientsData.Rows.Count > 0)
                                        {
                                            int success = new CrawlerDal().InsertIngredients(recipeIngredientsData, recipeId);
                                        }
                                    }
                                    if (!string.IsNullOrWhiteSpace(undetectedIngredients) && !string.IsNullOrEmpty(undetectedIngredients))
                                    {
                                        new CrawlerDal().UpdateudetectedINgredients(undetectedIngredients, recipeId);
                                    }
                                }
                                #endregion

                                isCrawlSuccess = true;
                            }
                        }
                    }
                    else
                    {
                        new CrawlerDal().LogError(_obj.Id, _obj.WebsiteName, recipeTitle, RecipeLink, "Unable to crawl the title page.", "Nigella Scrap Data");
                        crawlerStatus = "Error in crawling. Please check url povided or check error logs";
                        isCrawlSuccess = false;
                    }
                }
                catch (Exception ex)
                {
                    new CrawlerDal().LogError(_obj.Id, _obj.WebsiteName, recipeTitle, RecipeLink, ex.ToString(), "Nigella Scrap Data");
                    crawlerStatus = "Error in crawling. Please check url povided or check error logs";
                    isCrawlSuccess = false;
                    //return "Error in crawling. Please check url povided or check error logs";
                }

                if (Id != 0 && isCrawlSuccess)
                {
                    new CrawlerDal().UpdateCrawlableSiteConfiguration(LastCrawledRecipeName, "1", RecipeLink, _obj.BookId);
                    new CrawlerDal().InsertCrawlerRunStatus(_obj.Id, true, "Nigella Scrap Data");
                    crawlerStatus = "Your url has been crawled successfully. Please check main site to see changes";

                }
            }
            else
            {
                if (Id != 0 && !isCrawlSuccess)
                {
                    new CrawlerDal().InsertCrawlerRunStatus(_obj.Id, false, "Nigella Scrap Data");
                    new CrawlerDal().LogError(_obj.Id, _obj.WebsiteName, "", RecipeLink, "Please provide correct url to crawl", "Nigella Scrap Data");
                    crawlerStatus = "Error in crawling. Please check url povided or check error logs";
                }
            }

            return crawlerStatus;
        }

        #endregion
    }
}