﻿using EYBModel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.ComponentModel.DataAnnotations;

namespace EYBModel
{
   public class PdfBookMetaData : DbBaseModel
    {   
        public int MetaDataId { get; set; }
        public string BookName { get; set; }
        public string ISBN { get; set; }
        public string AuthorName { get; set; }
        public bool IndexingStatus { get; set; }
        public string ContainerName { get; set; }
        public string ThumbnailName { get; set; }
        public int MasterBookId { get; set; }


    }
}
